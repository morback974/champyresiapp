const johnnyFive = require('johnny-five');
let board = new johnnyFive.Board({port: getNamePort(), repl: false});
let listPin = [];
let listObservable = [];

board.on("close", () => {
    console.log('Redemarrage Connexion');
    board = null;
    setTimeout(() => {
        try{
            board = new johnnyFive.Board({port: getNamePort(), repl: false});
        }catch(err){
            process.exit(0);
        }
    }, 500);
});


async function getStatePin(pin, quantity = 1){
    if(!listPin[pin]){
        // creer acces Pin arduino et mise en place observable
        instancePin(pin);
    }

    return new Promise((resolve, reject) => {
        let observer = {value: [], quantity: quantity, next: (val) => {observer.value.push(val)}, complete: resolve, error: reject};
        listObservable[pin].subscribe(observer);
    })
    .catch((err) => {
        console.log("Erreur : board.js getStatePin", err);
    });
}

function setState(listVanne, duree = null){
    if(Array.isArray(listVanne)){
        listVanne.forEach(pin => {
            if(!listPin[pin]){
                instancePin(pin);
            }
        });
    }else{
        if(!listPin[listVanne]){
            instancePin(listVanne);
        }
    }
    let displayTime = require('./arduino').displayTime;
    console.log(displayTime(), `Activation Pin ${listVanne} pendant ${duree}`);
    return new Promise(async (resolve, reject) => {
        turnLow(listVanne);
        if(duree){
            setTimeout(() => {
                turnHigh(listVanne);
                resolve();
            },duree * 1000)
        }else{        
            resolve()
        }
    })
    .catch((err) => {
        console.log("Erreur : board.js setState", err);
    });
}

function write(pin, value){
    return new Promise((resolve, reject) => {
        if(!listPin[pin]){
            // creer acces Pin arduino et mise en place observable
            if(pin == 7){
                instancePin(pin, "led");
            }else{
                instancePin(pin);
            }
        }
    
        let displayTime = require('./arduino').displayTime;
        console.log(displayTime(), `Activation Pin ${pin} Valeur => ${value}`);
        listPin[pin].write(value);
        resolve();
    }).catch((err) => {
        console.log("write", err);
    });
}

function turnHigh(pins){
    if(Array.isArray(pins)){
        pins.forEach(pin => {
            listPin[pin].high();
        });
    }else{
        listPin[pins].high();
    }
}

function turnLow(pins){
    if(Array.isArray(pins)){
        pins.forEach(pin => {
            listPin[pin].low();
        });
    }else{
        listPin[pins].low();
    }
}

function getValAnalogique(pin){
    return new Promise((resolve, reject) => {
        let limitMesure = null;
        try{
            limitMesure = setTimeout(() => {
                reject(`Valeur Analogique indisponible pour le pin : ${pin}`);
            }, 5000);

            listPin[pin].query((state) => {
                clearTimeout(limitMesure);
                resolve(state.value);
            })
        }catch(err){
            reject(err);
        }
    }).catch((err) => {
        console.log("Erreur : board.js getValAnalogique", err);
    });
}

function instancePin(pin, type = null){
    if(type == null){
        listPin[pin] = new johnnyFive.Pin(pin);
        listPin[pin].high();
    }else{
        listPin[pin] = new johnnyFive.Led(7);
    }

    const pinObservable = require('./pinObservable');
    listObservable[pin] = new pinObservable(pin);
}

// Selectionne le port de la carte arduino
function getNamePort(){
    return process.platform == "win32" ? "COM3" : "/dev/ttyUSB0";
}

module.exports = {board: board,
    getStatePin: getStatePin,
    getValAnalogique: getValAnalogique,
    setState: setState,
    write: write
};