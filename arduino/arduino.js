const express = require('express');
const app = express();
const j5 = require('./board');

let status = "OK";
var cors = require('cors');

// use it before all route definitions
app.use(cors());
app.options('*', cors());
app.use(express.json())
app.use(function(req, res, next) {
    req.setTimeout(300000);
    next();
})

j5.board.on('ready', () => {
    console.log(displayTime(), "Connection serialPort OK");
    
    app.get("/getStatePin/:pin/:quantity", async (req, res) => {
        console.log(displayTime(), "Pin: " + req.params.pin, "Quantité mesure: " + req.params.quantity);
        let listValue = "";
        try{
            listValue = JSON.stringify(await j5.getStatePin(req.params.pin, req.params.quantity));
            res.end(listValue);
        }catch(err){
            console.log(err);
            res.end("[]");
        }
    });

    app.post("/switchState/",  async (req, res) => {
        try{
            await j5.setState(req.body.pin, req.body.duration);
            res.end();
        }catch(err){
            res.end();
        }
    });

    app.post("/writePin", async (req, res) => {
        try{
            await j5.write(req.body.pin, req.body.value);
            res.end();
        }catch(err){
            console.log(err);
            res.end();
        }
    });
});

app.get('/status', (req, res) => {
    let processTime = {
        up_time: Math.floor(process.uptime()),
        status: status
    };

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(processTime));
})

app.listen(8080, () => {
    console.log(displayTime(), "Lancement API Arduino");
})

process.setMaxListeners(0);

function displayTime() {
    var str = "";

    var currentTime = new Date()
    var hours = currentTime.getHours()
    var minutes = currentTime.getMinutes()
    var seconds = currentTime.getSeconds()

    if (minutes < 10) {
        minutes = "0" + minutes
    }
    if (seconds < 10) {
        seconds = "0" + seconds
    }
    str += hours + ":" + minutes + ":" + seconds + " ";

    return str;
}

module.exports = {status: status, displayTime: displayTime};