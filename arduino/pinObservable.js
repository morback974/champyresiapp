const j5 = require('./board');

const limitErrorVal = 30;

class pinObservable{
    constructor(pin){
        this.pin = pin;
        this.listObserver = []
        this.intState = null
        this.nbErrorVal = 0;
    }

    subscribe(observer){
        // add observer
        this.listObserver.push(observer);
        if(!this.intState){
            // start interval
            this.intState = setInterval(async () => {
                // getVal
                await j5.getValAnalogique(this.pin)
                .then((valAnalogique) => {
                    // add to all observer
                    this.listObserver.forEach(async (element, index) => {
                        element.next(valAnalogique);
                        element.quantity--;
                        // end observer when all value
                        if(element.quantity == 0){
                            element.complete(element.value);
                            this.listObserver.splice(index, 1);
                            if(this.listObserver.length == 0){
                                clearInterval(this.intState);
                                this.intState = null;
                            }
                        }
                    });
                })
                .catch((err) => {
                    // valeur incorrecte du pin
                    this.nbErrorVal++;
                    if(this.nbErrorVal == limitErrorVal){
                        process.exit(0);
                    }
                })
            }, 1000);
        }
    }
}

module.exports = pinObservable;