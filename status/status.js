const cron = require("node-cron");

const http = require('http');
const { spawn, exec } = require("child_process");

let champyresiBack = null;
let champyresiFront = null;

cron.schedule("* * * * *", async function() {
  if(process.platform == "win32"){
    await checkAccess('http://localhost:6000/status', "Master Co2", false)
    .catch(() => {
      launchMasterCo2();
    })
  }

  await checkAccess('http://localhost:3000/status', "Champyresi Back", false)
  .catch(() => {
    launchChampyresiBack();
  })
  .then(async () => {
    await checkAccess('http://localhost:4200/status', "Champyresi Front")
    .catch(() => {
      launchChampyresiFront();
    })
  })
});

let masterCo2 = null;
function launchMasterCo2(){
  masterCo2 = spawn("node", ["serveur.js"], {cwd: "../master"});

  masterCo2.stdout.on('data', (data) => {
    console.log('\x1b[33m%s\x1b[0m', data.toString());
  });

  masterCo2.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
  });
  
  masterCo2.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
  });
}

function launchChampyresiBack(){
  if(process.platform == "win32"){
    champyresiBack = spawn("node", ["serveur.js"], {cwd: "../app/back"});

    champyresiBack.stdout.on('data', (data) => {
      console.log('\x1b[36m%s\x1b[0m', data.toString());
    });

    champyresiBack.stderr.on('data', (data) => {
      console.error(`stderr: ${data}`);
    });
    
    champyresiBack.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  }else{
    champyresiBack = spawn("sudo", ["node", "serveur.js"], {cwd: "/home/pi/Desktop/champyresiApp/app/back"});
    champyresiBack.stdout.on('data', (data) => {
      console.log('\x1b[36m%s\x1b[0m', data.toString());
    });

    champyresiBack.stderr.on('data', (data) => {
      console.error(`stderr: ${data}`);
    });
    
    champyresiBack.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  }
}

function launchChampyresiFront(){
  if(process.platform == "win32"){
    champyresiFront = spawn("ng.cmd", ["serve"], {cwd: "../app/front/champyresi/src"});

    champyresiFront.stdout.on('data', (data) => {
      console.log('\x1b[35m%s\x1b[0m', data.toString());
    });

    champyresiFront.stderr.on('data', (data) => {
    });
    
    champyresiFront.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  }else{
    champyresiFront = spawn("sudo", ["ng", "serve"], {cwd: "/home/pi/Desktop/champyresiApp/app/front/champyresi/src"});
    champyresiFront.stdout.on('data', (data) => {
      console.log('\x1b[35m%s\x1b[0m', data.toString());
    });

    champyresiFront.stderr.on('data', (data) => {
    });
    
    champyresiFront.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
  }
}

let logUpTime = [];

setInterval(() => {
  console.log(logUpTime);
}, 600000)

function checkAccess(link, label, noMessage = true){
  return new Promise((resolve, reject) => {
    // Serveur Champyresi
    http.get(link,(resp) => {
      let data = '';

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        if(!noMessage){
          //console.log(label, data)
          logUpTime[label] = data;
        }
        resolve();
      });
    }).on("error", (err) => {
      console.log("Aucun access Serveur " + label);
      reject();
    }); 
  })
}