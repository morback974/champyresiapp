const path = require('path');
const cons = require('consolidate');
const co2Manager = require('./co2Manager');
const express = require('express');
const app = express();

const router = express.Router();

app.engine('html', cons.swig)
app.set('public', path.join(__dirname, '../../../public'));
app.set('view engine', 'html');
app.use(
    express.static(__dirname + '../../../public')
);
app.use(function(req, res, next) {
  req.setTimeout(0);
  next();
})
app.use('/', router);

app.get('/getCO2/:room', async (req, res) => {
    let co2 = await co2Manager.getCo2(req.params.room);

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(co2));
})

app.get('/test', async (req, res) => {
    console.log("test");

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end();
})

app.get('/status', async (req, res) => {
    let processTime = {
        up_time: Math.floor(process.uptime())
    };

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(processTime));
})

app.listen(6000, () => {
    console.log('Serveur Master Co2 lancer (port 6000)');
});