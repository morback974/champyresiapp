const gestionAir = require('./gestionAir/gestionAir').gestionAir;
const gestionHum = require('./gestionHum/gestionHum').gestionHum;
const gestionCo2 = require('./gestionCo2/gestionCo2').gestionCo2;
const raspberry = require('./raspberry/raspberry');

module.exports = function(app, express){
    var cors = require('cors');

    // use it before all route definitions
    app.use(cors());
    app.options('*', cors());
    app.use(express.json())
    app.use(function(req, res, next) {
        req.setTimeout(0);
        next();
    })
   
    app.get("/getStateGpio/:gpio/:quantity", async (req, res) => {
        let valueMesure = "";
        try{
            valueMesure = await raspberry.gpioCan(req.params.gpio, req.params.quantity)
            .catch((err) => {
                console.log("Probleme mesure router", req.params.gpio, req.params.quantity);
            });

            res.end(JSON.stringify(valueMesure * 40));
        }catch(err){
            console.log("/getStateGpio/:gpio/:quantity", err)
            res.end("[]");
        }
    });
    
    app.get("/getEtal/:lib", async (req, res) => {
        let etal = null;
        switch(req.params.lib){
            case 'etalAir': etal = await gestionAir.getLastEtalAir();
                break;
            case 'etalHum': etal = await gestionHum.getLastEtalHum();
                break;
            case 'etalSec': etal = await gestionHum.getLastEtalSec();
                break;
        }
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        
        res.end(JSON.stringify(etal));
    })

    app.get("/getStateRelay", async (req, res) => {
        raspberry.getStateRelay();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end();
    })

    app.get("/getConfigPin", async (req, res) => {
        const fs = require('fs');
        let fichier = fs.readFileSync('./config/pin.json', 'utf8');
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');

        res.end(fichier);
    })

    app.get("/getTemperatureSec", async (req, res) => {
        let dataAir = await gestionHum.getTemperatureSec();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        
        res.end(JSON.stringify(dataAir));
    })

    app.get("/getTemperatureHum", async (req, res) => {
        let dataAir = await gestionHum.getTemperatureHum();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        
        res.end(JSON.stringify(dataAir));
    })

    app.get("/getConsigneHum", async (req, res) => {
        let dataAir = await gestionHum.getConsigneHum();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        
        res.end(JSON.stringify(dataAir));
    })
    
    app.get("/getConsigneCo2", async (req, res) => {
        let dataAir = await gestionCo2.getConsigneCo2();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        
        res.end(JSON.stringify(dataAir));
    })
    
    app.get("/getLastTauxCo2", async (req, res) => {
        let dataAir = await gestionCo2.getLastTauxCo2();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(dataAir));
    })

    app.get("/getLastTauxHumidite", async (req, res) => {
        let dataAir = await gestionHum.getLastTauxHumidite();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(dataAir));
    })

    app.get("/getLastTemperatureAir", async (req, res) => {
        let dataAir = await gestionAir.getLastTemperatureAir();
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(dataAir));
    })

    app.get("/getConsigneAir", async (req, res) => {
        let consigneAir = await gestionAir.getConsigneAir();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(consigneAir));
    })

    app.get("/getObjAir", async (req, res) => {
        let objAir = await gestionAir.getObjAir();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(objAir));
    })

    app.get("/getVanneAir", async (req, res) => {
        let vanneAir = await gestionAir.getEtatVanneAir();
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(vanneAir));
    })

    app.get("/getPasAir", async (req, res) => {
        let pasAir = await gestionAir.getPasAir();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(pasAir));
    })

    app.get("/getStatusObjAir", async (req, res) => {
        let statusObjAir = await gestionAir.getVarConsigneAirActif();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(statusObjAir));
    })

    app.get("/getNbJour", async (req, res) => {
        const serveur = require('./serveur').status;
        let nbJour = await serveur.getNbJour();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(nbJour));
    })

    app.get("/getSuiviProcess", async (req, res) => {
        const serveur = require('./serveur').status;
        let suiviProcess = await serveur.getSuiviProcess();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(suiviProcess));
    })

    app.get("/getSuiviSousProcess", async (req, res) => {
        const serveur = require('./serveur').status;
        let suiviSousProcess = await serveur.getSuiviSousProcess();
   
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(suiviSousProcess));
    })

    app.get("/resetApp", async (req, res) => {
        const serveur = require('./serveur').status;
        serveur.resetCycle();
        gestionAir.resetApp();
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end();
    })

    app.post("/modifEtal", (req, res) => {
        switch(req.body.lib){
            case 'etalAir': gestionAir.modifEtal(req.body.quantity);
                break;
            case 'etalHum': gestionHum.modifEtalHum(req.body.quantity);
                break;
            case 'etalSec': gestionHum.modifEtalSec(req.body.quantity);
                break;
        }

        res.status(200).end();
    })

    app.post("/modifConsigneAir", (req, res) => {
        gestionAir.modifConsigneAir(req.body.quantity);
        res.status(200).end();
    })

    app.post("/switchRelay", (req, res) => {
        raspberry.switchRelay(req.body.relay, req.body.etat, req.body.duration);
        res.status(200).end();
    })

    app.post("/modifConsigneHum", (req, res) => {
        gestionHum.modifConsigneHum(req.body.quantity);
        res.status(200).end();
    })

    app.post("/modifConsigneCo2", (req, res) => {
        gestionCo2.modifConsigneCo2(req.body.quantity);
        res.status(200).end();
    })
    
    app.post("/setConsigneAir", (req, res) => {
        gestionAir.setConsigneAir(req.body.quantity);
        res.status(200).end();
    })

    app.post("/modifVanneAir", (req, res) => {console.log("/modifVanneAir", req.body.val)
        gestionAir.modifVanneAir(req.body.val);
        res.status(200).end();
    })

    app.post("/setPas", (req, res) => {
        gestionAir.setPas(req.body.quantity);
        res.status(200).end();
    })
    
    app.post("/setObjAir", (req, res) => {
        gestionAir.setObjAir(req.body.quantity);
        res.status(200).end();
    })

    app.post("/actifBrume", (req, res) => {
        gestionHum.actifBrume(req.body.val);
        res.status(200).end();
    })

    app.post("/desactifBrume", (req, res) => {
        gestionHum.desactifBrume();
        res.status(200).end();
    })

    app.get('/status', (req, res) => {
        let processTime = {
            up_time: Math.floor(process.uptime())
        };
    
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(processTime));
    })
}