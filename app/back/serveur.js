let timingJour = new Date();

try{
    const {delay, hToMs} = require('./util/util');
    const gestionAir = require('./gestionAir/gestionAir').gestionAir;
    const gestionHum = require('./gestionHum/gestionHum').gestionHum;
    const gestionCo2 = require('./gestionCo2/gestionCo2').gestionCo2;
    const express = require('express');
    const app = express();
    let http = require('http').Server(app);
    
    let socket = require('./routerSocket');
    socket.init(http);
    
    require('./router')(app, express);

    const db = require("./bdd/connection");
    
    http.listen(3000, () => {
        console.log("Lancement Serveur Champyresi");
    })
    
    let dataServ = {};
    dataServ = new Proxy(dataServ, {
        set(obj, prop, value) {
            if (prop === 'suiviProcess') {
                console.log('suiviProcess', value)
                socket.emit('suiviProcess', value);  
            } else if(prop == 'nbJour'){
                socket.emit('nbJour', value);
            }else if(prop == 'suiviSousProcess'){
                socket.emit('suiviSousProcess', value);
            }
            return Reflect.set(...arguments);
          }
    })
    let continueApp = true;

    delay(5, "seconde").then(()=> {
        launchCron();
        launchApp();
    })
    
    async function launchApp(){
        dataServ.suiviProcess = "Lancement Gestion Champignon";
        dataServ.suiviSousProcess = "";
    
        await delay(1, "seconde");
    
        await gestionAir.initData();
        await gestionHum.initData();
        await gestionCo2.initData();
    
        gestionCo2.launch();
    
        while(continueApp){
            
            // Partie 1 Gestion Air
            await gestionAir.launch()
            .catch((err) => {
                console.log("gestionAir.launch()",err);
            })
            
            await delay(1, "seconde");
            
            await gestionHum.launch()
            .catch((err) => {
                console.log(gestionHum.launch(), err);
            })
    
            dataServ.suiviProcess = "Attente prochain cycle";
            updateSousStatus(300, "Temps restant avant prochain Cycle");
            await delay(5, "minute");
        }
    }
    
    function updateSousStatus(duree, libelle){
        let dureeActionRestante = duree;
    
        let intMess = setInterval(() => {
            dataServ.suiviSousProcess = `${libelle} (temps restant ${dureeActionRestante--}/${duree})`;
        }, 1000)
    
        setTimeout(() => {
            clearInterval(intMess);
            dataServ.suiviSousProcess = "";
        }, (duree + 1) * 1000);
    }
    
    async function launchCron(){
    
        initDataServ();
        updateJour();
    
        async function updateJour(){
            let timeInterval = require('./bdd/timeInterval');
            await timeInterval.getEvolNbJourInterval().then((val) => {
                let evolNbJour = Date.parse(val);
    
                if(Date.now() - evolNbJour > hToMs(24)){
                    dataServ.nbJour++;
                    let gestionCycle = require('./bdd/cycle');
                    gestionCycle.selectLastCycle()
                    .then((valeur) => {
                        valeur = valeur[0];
                        gestionCycle.insertCycle({nbCycle: valeur.nbCycle, nbJour: dataServ.nbJour});
                    })
    
                    timeInterval.initChangeJour();
                }

                setTimeout(() => {
                    updateJour();
                }, hToMs(24) - (Date.now() - evolNbJour));
    
                return 0;
            }).catch((err) => {
                // nada
                console.log("update jour",err);
            });
        }
    }
    
    async function initDataServ(){
        dataServ.nbJour = 1;
        return new Promise((resolve, reject) => {
            require('./bdd/cycle').selectLastCycle()
            .then((result) => {
                result = result[0];
                dataServ.nbJour = result.nbJour;
                resolve();
            });
        })
    }
    
    dataServ.getNbJour = () => {
        return new Promise((resolve, reject) => {
            let intServ = setInterval(() => {
                if(dataServ != null){
                    if(dataServ.nbJour){
                        clearInterval(intServ);
                        resolve(dataServ.nbJour);
                    }
                }
            });
        });
    }
    
    dataServ.getSuiviProcess = () => {
        return new Promise((resolve, reject) => {
            let intServ = setInterval(() => {
                if(dataServ != null){
                    if(dataServ.suiviProcess){
                        clearInterval(intServ);
                        resolve(dataServ.suiviProcess);
                    }
                }
            });
        });
    }
    
    dataServ.getSuiviSousProcess = () => {
        return new Promise((resolve, reject) => {
            let intServ = setInterval(() => {
                if(dataServ != null){
                    if(dataServ.suiviSousProcess){
                        clearInterval(intServ);
                        resolve(dataServ.suiviSousProcess);
                    }
                }
            });
        });
    }
    
    dataServ.resetCycle = async () => {
        // let cycleJ = require('./bdd/cycle');
        // let lastCycle = await cycleJ.selectLastCycle();
        // console.log(lastCycle.nbCycle)
        // dataServ.nbJour = 1;
        // cycleJ.insertCycle({nbCycle:lastCycle.nbCycle + 1, nbJour: 1});
        dataServ.nbJour = 1;
    }
    
    module.exports = {status: dataServ, updateSousStatus: updateSousStatus};
}catch(err){

}