const raspberry = require('../raspberry/raspberry');
let serveurMod = null;

let gestionAir = {};
let dataAir = null;
let gpioAir = configPin("gpioAir");
let gpioAirOn = configPin("gpioAirOn");
let gpioAirOff = configPin("gpioAirOff");

gestionAir.launch = async () => {
    serveurMod = require('../serveur');

    if(!dataAir){
        dataAir = {};
        dataAir = await initProxyAir();
    }

    serveurMod.status.suiviProcess = "Lancement Gestion Temperature Air";

    return new Promise(async (resolve, reject) => {
        let tempAir = await mesureAir()
        .catch((err) => {
            console.log("probleme lors de la mesure Air", err);
            reject();
        });
        dataAir.temperatureAir = tempAir;
        console.log("Mesure Air : ", dataAir.getTemperature(), tempAir);
        let deltaTemp = dataAir.getTemperature() - dataAir.consigneAir;
        let dureeAction = dureeRegulationAir(deltaTemp);

        if(dureeAction > 0){
            if(deltaTemp > 0){
                gestionAir.modifVanneAir(dureeAction);
            }else{
                gestionAir.modifVanneAir(-dureeAction);
            }
        }else{
            serveurMod.status.suiviSousProcess = "Temperature ~= consigne, Aucune Action", "suiviSousProcess";
            //saveActionAir();
        }

        resolve();
    }).catch((err) => {
        console.log("probleme lors de la gestion Air", err);
    });
}

gestionAir.initData = async () => {
    if(!dataAir){
        // recup bdd valeur
        // TODO
        dataAir = {};
        dataAir = await initProxyAir();
        launchCron();
    }
}

gestionAir.getLastTemperatureAir = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.temperatureAir){
                    clearInterval(intAir);
                    resolve(dataAir.getTemperature());
                }
            }
        });
    });
}

gestionAir.getLastEtalAir = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.etalAir){
                    clearInterval(intAir);
                    resolve(dataAir.etalAir);
                }
            }
        });
    });
}

gestionAir.getConsigneAir = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.consigneAir){
                    clearInterval(intAir);
                    resolve(dataAir.consigneAir);
                }
            }
        })
    })
}

gestionAir.getObjAir = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.objAir){
                    clearInterval(intAir);
                    resolve(dataAir.objAir);
                }
            }
        })
    })
}


gestionAir.getPasAir = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.pasAir){
                    clearInterval(intAir);
                    resolve(dataAir.pasAir);
                }
            }
        })
    })
}


gestionAir.getVarConsigneAirActif = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.varConsigneAirActif){
                    clearInterval(intAir);
                    resolve(dataAir.varConsigneAirActif);
                }
            }
        })
    })
}

gestionAir.getEtatVanneAir = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataAir != null){
                if(dataAir.etatVanneFroid != null){
                    clearInterval(intAir);
                    resolve(dataAir.etatVanneFroid);
                }
            }
        })
    })
}

gestionAir.modifConsigneAir = (quantity) => {
    dataAir.consigneAir += quantity;
}

gestionAir.modifVanneAir = async (val) => {
    let intVanne = null;

    if(val > 0){
        raspberry.switchRelay(gpioAirOff, 0, val)
        .catch((err) => {
            console.log("Probleme fermeture Vanne Air");
        })
        raspberry.switchRelay(gpioAirOn[1], 0, val)
        .catch((err) => {
            console.log("Probleme ouverture Vanne Air");
        })
        intVanne = setInterval(() => {
            dataAir.etatVanneFroid++;
        }, 1000)

        setTimeout(() => {
            clearInterval(intVanne);
        }, val * 1000 + 1000)
    }else{
        raspberry.switchRelay(gpioAirOff, 0, -val)
        .catch((err) => {
            console.log("Probleme fermeture Vanne Air", err);
        })
        intVanne = setInterval(() => {
            dataAir.etatVanneFroid--;
        }, 1000)

        setTimeout(() => {
            clearInterval(intVanne);
        }, -val * 1000 + 1000)
    }
}

gestionAir.modifEtal = (quantity) => {
    dataAir.etalAir += quantity;
}

gestionAir.setConsigneAir = (quantity) => {
    dataAir.consigneAir = quantity;
}

gestionAir.setPas = (pas) => {
    dataAir.pasAir = pas;
}

gestionAir.setObjAir = (pas) => {
    dataAir.objAir = pas;
    dataAir.varConsigneAirActif = true;
}

gestionAir.resetApp = () => {
    dataAir.consigneAir = 20;
    dataAir.objAir = 20;
    dataAir.pasAir = 0;
    dataAir.varConsigneAirActif = false;
}

async function initProxyAir(){
    dataAir.etatVanneFroid = 0;
    dataAir.temperatureAir = 13;
    dataAir.consigneAir = 13;
    dataAir.objAir = 0;
    dataAir.pasAir = 0;
    dataAir.varConsigneAirActif = false;
    dataAir.idCycle = 1;
    dataAir.etalAir = 0;

    return new Promise((resolve, reject) => {
        let bddGestionAir = require('./../bdd/gestionAir');
        bddGestionAir.selectLastGestionAir()
        .then((val) => {
            val = val[0];
            dataAir = {...dataAir, ...val}

            rajoutFunction();
    
            let socket = require('../routerSocket');
            resolve(new Proxy(dataAir, {
                set(obj, prop, value){
                    if(prop == "etatVanneFroid"){
                        if(value < 0){
                            value = 0;
                        }else if(value > 40){
                            value = 40;
                        }
                        socket.emit("etatVanneFroid", value);
                        Reflect.set(...arguments);
                    }else if(prop == "temperatureAir"){
                        Reflect.set(...arguments);
                        socket.emit("temperatureAir", dataAir.getTemperature());
                    }else if(prop == "consigneAir"){
                        socket.emit("consigneAir", value);
                        Reflect.set(...arguments);
                    }else if(prop == "etalAir"){
                        socket.emit("etalAir", value);
                        Reflect.set(...arguments);
                    }else if(prop == "pasAir"){
                        socket.emit("pasAir", value);
                        Reflect.set(...arguments);
                    }else if(prop == "objAir"){
                        socket.emit("objAir", value);
                        Reflect.set(...arguments);
                    }else if(prop == "varConsigneAirActif"){
                        socket.emit("varConsigneAirActif", value);
                        Reflect.set(...arguments);
                    }else{
                        Reflect.set(...arguments);
                    }
                    saveGestionAir();
                }
            }));
        });    

    }).catch((err) => {
        console.log("initProxyAir", err);
    })
}

function launchCron(){
    setInterval(() => {
        if(dataAir.varConsigneAirActif){
            if(dataAir.consigneAir > dataAir.objAir){
                dataAir.consigneAir -= (dataAir.pasAir / 1440);
                if(dataAir.consigneAir <= dataAir.objAir){
                    dataAir.consigneAir = dataAir.objAir;
                    dataAir.varConsigneAirActif = false;
                }
            }else{
                dataAir.consigneAir += (dataAir.pasAir / 1440);
                if(dataAir.consigneAir >= dataAir.objAir){
                    dataAir.consigneAir = dataAir.objAir;
                    dataAir.varConsigneAirActif = false;
                }
            }
        }
    }, 30000)
}

function rajoutFunction(){
    dataAir.getTemperature = () => {
        return dataAir.temperatureAir + dataAir.etalAir;
    }
}

// function saveActionAir(dureeAction, infoAction){
//     saveGestionAir()
//     .then((val) => {
//         let dataAirClean = {};

//         dataAirClean.etatVanneFroidAfter = dataAir.etatVanneFroid;

//         dataAirClean.idMesureAir = val;

//         let bddActionAir = require('./../bdd/actionAir');
//         bddActionAir.insertActionAir(dataAirClean);
//     })
// }

function saveGestionAir(){
    return new Promise((resolve, reject) => {
        let dataAirClean = {};
        if(dataAir.temperatureAir < 0 || dataAir.temperatureAir > 40 || !dataAir.temperatureAir || isNaN(dataAir.temperatureAir)){
            console.log("Enregistrement DataAir Annulé cause temperature Air: " + dataAir.temperatureAir);
            reject();
        }else{
            dataAirClean.temperatureAir = dataAir.getTemperature();
        }
    
        if(dataAir.consigneAir < 0 || dataAir.consigneAir > 40 || !dataAir.consigneAir){
            console.log("Enregistrement DataAir Modifier cause consigne Air: " + dataAir.consigneAir);
            console.log("Update Consigne Air: 20");
            dataAir.consigneAir = 20;
            reject();
        }else{
            dataAirClean.consigneAir = dataAir.consigneAir;
        }

        if(dataAir.etalAir < -40 || dataAir.etalAir > 40 || dataAir.etalAir == null){
            console.log("Enregistrement DataAir Modifier cause etalonnage air: " + dataAir.etalAir);
            console.log("Update etalonnage air: 0");
            dataAir.etalAir = 0;
            reject();
        }else{
            dataAirClean.etalAir = dataAir.etalAir;
        }

        if(dataAir.objAir < 10 || dataAir.objAir > 40 || dataAir.objAir == null){
            console.log("Enregistrement DataAir Modifier cause objectif air: " + dataAir.objAir);
            console.log("Update objectif air: " + dataAir.consigneAir);
            dataAir.objAir = dataAir.consigneAir;
            reject();
        }else{
            dataAirClean.objAir = dataAir.objAir;
        }
        
        if(dataAir.pasAir < -40 || dataAir.pasAir > 40 || dataAir.pasAir == null){
            console.log("Enregistrement DataAir Modifier cause pas air: " + dataAir.pasAir);
            console.log("Update pas air: 0");
            dataAir.pasAir = 0;
            reject();
        }else{
            dataAirClean.pasAir = dataAir.pasAir;
        }

        if(dataAir.varConsigneAirActif == null){
            console.log("Enregistrement DataAir Modifier cause etatObjectif air: " + dataAir.varConsigneAirActif);
            console.log("Update etatObjectif air: Deactiver");
            dataAir.varConsigneAirActif = false;
            reject();
        }else{
            dataAirClean.varConsigneAirActif = dataAir.varConsigneAirActif;
        }

        resolve(dataAirClean);
    })
    .then((data) => {
        try{
            let bddGestionAir = require('./../bdd/gestionAir');
            bddGestionAir.insertGestionAir(data)
            .then((val) => {
            })
            .catch((err) => {
                console.log("err gestionAir", err)
            });
        }catch(err){
            console.log(err);
        }
    })
    .catch((err) => {

    })
}

function dureeRegulationAir(deltaTemp){
    let dureeAction = 0;

    if(deltaTemp > 1.5){
        dureeAction = 40;
    }else if(deltaTemp > 1){
        dureeAction = 15;
    }else if(deltaTemp > 0.5){
        dureeAction = 5;
    }else if(deltaTemp > 0.3){
        dureeAction = 2;
    }

    if(deltaTemp < -1.5){
        dureeAction = 40;
    }else if(deltaTemp < -1){
        dureeAction = 15;
    }else if(deltaTemp < -0.5){
        dureeAction = 5;
    }else if(deltaTemp < -0.3){
        dureeAction = 2;
    }

    return dureeAction;
}

function mesureAir(){
    return new Promise(async (resolve, reject) => {
        try{
            const raspberry = require('../raspberry/raspberry');
            let valueMesure = await raspberry.gpioCan(gpioAir, 30)
            .catch((err) => {
                console.log("Probleme mesure Air")
            });
            console.log("FMesureAir", valueMesure);
            valueMesure *= 5 * 40 / 5;
            if(valueMesure < 10 || valueMesure > 40){
                reject("Probleme de Temperature air")
            }else{
                resolve(Number(valueMesure));
            }
        }catch(err){
            console.log(err);
            reject(err);
        }
    }).catch((err) => {
        console.log("mesureAir", err);
    });    
}

function configPin(pin){
    const fs = require('fs');
        
    let fichier = fs.readFileSync('./config/pin.json');
    let infoPin = JSON.parse(fichier);

    return infoPin.sensorTemperature[pin];
}

module.exports = {gestionAir: gestionAir, dataAir: dataAir};