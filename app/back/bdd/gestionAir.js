const db = require('./connection');

const insertGestionAir = (data) => {
    return new Promise((resolve, reject) => {
        selectLastGestionAir()
        .then((valeur) => {
            let id = 1;
            if(valeur.length > 0){
                id = valeur[0].id + 1;
            }

            data.mesuredAt = new Date();
            let gestionCycle = require('./cycle');
            gestionCycle.selectLastCycle()
            .then((cycle) => {
                
                let sql = "insert into gestionAir (id, temperatureAir, consigneAir, etalAir, objAir, pasAir, varConsigneAirActif, mesuredAt, idCycle) values ("+ id +","+ data.temperatureAir +","+ data.consigneAir +","+ data.etalAir +","+ data.objAir +","+ data.pasAir +","+ data.varConsigneAirActif +",'" + new Date() +"', " + cycle[0].id +");";
                db.launchSQL(sql, null, true).then((result) => {
                    // resolve({id:result.insertId, data:data});
                    resolve()
                })
                .catch(() => {
                    reject();
                })
            })
        })

    })
}

const selectLastGestionAir = () => {
    return new Promise((resolve, reject) => {
        let gestionCycle = require('./cycle');
        gestionCycle.selectLastCycle()
        .then((valeur) => {
            let sql = "";

            sql = "select * from gestionAir order by id desc limit 1;";
           
            db.launchSQL(sql).then((result) => {
                // console.log("selectLastGestionAir", result)
                resolve(result);
            })
        })
        .catch((err) => {
            console.log("selectLastGestionAir", err)
        })
    });
}

const selectLastByQuantity = (quantity = null) => {
    return new Promise ((resolve, reject) => {
        let gestionCycle = require('./cycle');
        gestionCycle.selectLastCycle()
            .then((valeur) => {
                let sql = "";
                if(!quantity){
                    sql = `select * from gestionAir left join cycle on gestionAir.idCycle = cycle.id left join actionAir on gestionAir.id = actionAir.idMesureAir where cycle.nbCycle = ${valeur.nbCycle} order by gestionAir.id desc;`;
                }else{
                    sql = `select * from gestionAir left join cycle on gestionAir.idCycle = cycle.id left join actionAir on gestionAir.id = actionAir.idMesureAir where cycle.nbCycle = ${valeur.nbCycle} order by gestionAir.id desc limit ${quantity};`;
                }

                con.query(sql, (err, result) => {
                    resolve(result);
                })
            })
    });
}

const selectAll = () => {
    return new Promise((resolve, reject) => {
        let sql = "select * from gestionair";

        db.launchSQL(sql).then((result) => {
            resolve(result);
        })
    });
}

module.exports = {
    selectAll: selectAll,
    insertGestionAir: insertGestionAir,
    selectLastGestionAir: selectLastGestionAir,
    selectLastByQuantity: selectLastByQuantity
}