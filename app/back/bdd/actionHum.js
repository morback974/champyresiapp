const con = require('./connection').con;
const {saveMesure} = require('./gestionHum');

const saveAction = async(data, otherData) => {
    await saveMesure(data).then((dataInserted) => {
        checkDataActionHum(data, otherData, dataInserted).then((dataChecked) => {
            insertActionHum(dataChecked).catch((err) => {
                console.log(err);
            })
        }).catch((err) => {
            console.log(err);
        })
    });
}

const checkDataActionHum = (data, otherData, dataInserted) => {
    return new Promise(async (resolve, reject) => {
        let listErreur = [];
        let dataChecked = {};

        let deltaHum = await data.getDeltaHumidite();
        dataChecked.deltaHum = deltaHum;

        dataChecked.tempsDesHum = otherData.desHum ? otherData.desHum : 0;
        dataChecked.tempsBrume = otherData.brume ? otherData.brume : 0;

        dataChecked.idMesureHum = dataInserted.id;

        if(listErreur.length > 0){
            reject(listErreur);
        }else{
            resolve(dataChecked);
        }
    }).catch((err) => {
        console.log("checkDataActionHum", err);
    })
}

const insertActionHum = (data) => {
    return new Promise((resolve, reject) => {
        data.launchedAt = new Date();

        let sql = "insert into actionHum set ?";

        con.query(sql, data, (err, result) => {
            if(err){
                reject(err);
            }

            resolve();
        })
    }).catch((err) => {
        console.log("insertActionHum", err);
    })
}

module.exports = {
    saveAction: saveAction
}