const db = require('./connection');

const createLabel = (libelle) => {
    return new Promise((resolve, reject) => {
        let dateString = todayMysql();
        let sql = "insert into timeinterval (libelle, lastactivation) values ('" + libelle + "', '" + dateString + "');";

        db.launchSQL(sql).then(() => {
            resolve();
        })
    });
}

const exist = (libelle) => {
        return new Promise ((resolve, reject) => {
            let sql = "select * from timeinterval where libelle = '" + libelle + "';";

            db.launchSQL(sql).then((result) => {
                if(result.length > 0){
                    resolve(true);
                }else{
                    resolve(false);
                }
            });
        })
    }

async function initEvolConsigne(){
    await initByLibelle("evolConsigne");
}

async function initChangeJour(){
    await initByLibelle("nbJour");
}

async function initInsertData(){
    await initByLibelle("insertData");
}

async function initTempsBrume(){
    await initByLibelle("tempsBrume");
}

async function initGestionHumidite(){
    await initByLibelle("gestionHumidite");
}

async function initGestionCo2(){
    await initByLibelle("gestionCo2");
}

async function initByLibelle(libelle){
    if(!await exist(libelle)) 
        return createLabel(libelle);
    else{
        return new Promise((resolve, reject) =>{
            let sql = "update timeinterval set lastactivation = '" + todayMysql() + "' where libelle = '" + libelle + "';";
            db.launchSQL(sql).then(() => {
                resolve();
            })
        });
    }
}

async function getEvolConsigneInterval(){
    return getIntervalByLibelle("evolConsigne");
}

async function getEvolNbJourInterval(){
    return getIntervalByLibelle("nbJour");
}

async function getEvolInsertDataInterval(){
    return getIntervalByLibelle("insertData");
}

async function getTempsBrumeInterval(){
    return getIntervalByLibelle("tempsBrume");
}

async function getGestionHumidite(){
    return getIntervalByLibelle("gestionHumidite");
}

async function getGestionCo2(){
    return getIntervalByLibelle("gestionCo2");
}

function getIntervalByLibelle(libelle){
    return new Promise((resolve, reject) => {
        let sql = "select lastActivation from timeinterval where libelle = '" + libelle + "';";
        
        db.launchSQL(sql).then((result) => {
            if(result.length > 0){
                resolve(result[0].lastactivation + ".000Z");
            }else{
                createLabel(libelle);
                reject();
            }
        });
    }).catch(async () => {
        return await getIntervalByLibelle(libelle);
    });
}

function todayMysql(){
    let dateNow = new Date();
    return dateNow.toJSON().substr(0, dateNow.toJSON().length - 5);
}



module.exports = {
    initEvolConsigne: initEvolConsigne,
    initChangeJour: initChangeJour,
    initInsertData: initInsertData,
    initTempsBrume:initTempsBrume,
    initGestionHumidite: initGestionHumidite,
    initGestionCo2: initGestionCo2,
    getEvolConsigneInterval: getEvolConsigneInterval,
    getEvolNbJourInterval: getEvolNbJourInterval,
    getEvolInsertDataInterval: getEvolInsertDataInterval,
    getTempsBrumeInterval:getTempsBrumeInterval,
    getGestionHumidite: getGestionHumidite,
    getGestionCo2: getGestionCo2
}