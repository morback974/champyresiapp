const db = require('./connection');

const insertCycle = (data) => {
    return new Promise((resolve, reject) => {
        selectLastCycle()
            .then((valeur) => {
                let id = 1;
                if(valeur.length > 0){
                    id = valeur[0].id + 1;
                }
                let sql = "insert into cycle (id, nbCycle, nbJour) VALUES (" + id + "," + data.nbCycle + ", " + data.nbJour + ");";

                db.launchSQL(sql).then((result) => {
                    resolve();
                }).catch((err) => {
                    if(err){
                        reject(err);
                    }
                })
            })

    }).catch((err) => {
        console.log("insertCycle", err);
    })
}

const selectLastCycle = () => {
    return new Promise((resolve, reject) => {
        let sql = "select * from cycle order by id desc limit 1;";

        db.launchSQL(sql).then((val) => {
            resolve(val);
        })
    }).catch((err) => {
        console.log("selectLastCycle", err);
    })
}

module.exports = {
    insertCycle: insertCycle,
    selectLastCycle: selectLastCycle
}