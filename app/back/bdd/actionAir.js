const con = require('./connection').con;
const {saveMesure} = require('./gestionAir');

const insertActionAir = (data) => {
    return new Promise((resolve, reject) => {
        data.launchedAt = new Date();

        let sql = "insert into actionAir set ?";

        con.query(sql, data, (err, result) => {
            if(err){
                reject(err);
            }

            resolve({id:result.insertId, data:data});
        })
    }).catch((err) => {
        console.log("insertActionAir", err);
    })
}

module.exports = {
    insertActionAir: insertActionAir
}