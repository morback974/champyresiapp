const sqlite3 = require('sqlite3').verbose();
const path = require('path');

let db = new sqlite3.Database(path.dirname(require.main.filename) + '/champyresi.db', (info) => {
    //console.log("info", info);
});

start();
async function start(){
    try{
        await createTabletimeinterval();
        await createTableCycle();
        await cycleExist();
        await createTableGestionAir();
        // createTableActionAir();
    }catch(err){
        console.log("start", err);
    }
}

// try{
//     con.connect(async (err) => {
//         if(err) throw err;
//         console.log("Connection Database");
    
//         // await tableExist('log', createTableLog);
//         await tableExist('timeinterval', createTabletimeinterval);
//         await tableExist('cycle', createTableCycle);
//         await cycleExist();

//         await tableExist('gestionAir', createTableGestionAir);
//         await tableExist('actionAir', createTableActionAir);

//         await tableExist('gestionHum' , createTableGestionHum);
//         // await tableExist('actionHum', createTableActionHum);
//     });
// }
// catch(err){
//     console.log("Erreur lors de la tentative de connexion bdd mysql");
// }


function tableExist(nameTable, cb){
    let sql = `SELECT table_name FROM information_schema.tables WHERE table_schema = 'champyresi' AND table_name = '${nameTable}';`
    return launchSQL(sql).then((val) => {
        if(val.length == 0){
            return cb();
        }
    });
}

function cycleExist(){
    let sql = "SELECT * FROM cycle ORDER BY id desc LIMIT 1;";
    return launchSQL(sql).then((val) => {
        if(val.length == 0){
            let insert = "insert into cycle (nbCycle, nbJour) VALUES (1, 1);";
            return launchSQL(insert);
        }
    })
}

async function createTableLog(){
    let sql = "create table log (id int not null auto_increment primary key, dataArduino text);";
    await launchSQL(sql);
}
function todayMysql(){
    let dateNow = new Date();
    return dateNow.toJSON().substr(0, dateNow.toJSON().length - 5);
}
async function createTabletimeinterval(){
    let sql = "create table if not exists timeinterval (libelle text, lastactivation datetime);";
    await launchSQL(sql);
}

async function createTableGestionAir(){
    let sql = "create table if not exists gestionAir (id int primary key, mesuredAt datetime, temperatureAir float, consigneAir float, objAir float, pasAir float, etalAir float, varConsigneAirActif boolean, idCycle int);";
    await launchSQL(sql);
}

async function createTableActionAir(){
    let sql = "create table if not exists actionAir (id int not null primary key, launchedAt datetime, deltaAir float, etatVanneFroidBefore float, etatVanneFroidAfter float, idMesureAir int);";
    await launchSQL(sql);
}

async function createTableCycle(){
    let sql = "create table if not exists cycle (id int primary key, nbCycle int, nbJour int);";
    await launchSQL(sql);
}

async function createTableGestionHum(){
    let sql = "create table gestionHum (id int not null auto_increment primary key, mesuredAt datetime, temperatureSec float, temperatureHum float, tauxHumidite float, consigneHum float, objHum float, pasHum float, objHumActif boolean, idCycle int)";
    await launchSQL(sql);
    sql = "ALTER TABLE `gestionhum` ADD COLUMN `etalHum` FLOAT NULL DEFAULT NULL AFTER `idCycle`, ADD COLUMN `etalSec` FLOAT NULL DEFAULT NULL AFTER `etalHum`;";
    await launchSQL(sql);
}

async function createTableActionHum(){
    let sql = "create table actionHum (id int not null auto_increment primary key, launchedAt datetime, deltaHum float, tempsDeshum float, tempsBrume float, idMesureHum int)";
    await launchSQL(sql);
}

async function launchSQL(sql, data = null, run = false){
    if(run){
        if(!data){
            return new Promise((resolve, reject) => {
                db.run(sql, [], (err) => {
                    if(err){
                        reject(err);
                    }
                    resolve();
                })
            }).catch((err) => {
                // console.log("launchSQL", err);
            })
        }else{
            return new Promise((resolve, reject) => {
                db.run(sql, data, (err) => {
                    if(err){
                        reject(err);
                    } console.log(`A row has been inserted with rowid ${this.lastID}`);
                    resolve();
                })
            }).catch((err) => {
                // console.log("launchSQL", err);
            })
        }
    }else{
        if(!data){
            return new Promise((resolve, reject) => {
                db.all(sql, [], (err, result) => {
                    if(err){
                        reject(err);
                    }
                    resolve(result);
                })
            }).catch((err) => {
                // console.log("launchSQL", err);
            })
        }else{
            return new Promise((resolve, reject) => {
                db.all(sql, data, (err, result) => {
                    if(err){
                        reject(err);
                    } 
                    resolve(result);
                })
            }).catch((err) => {
                // console.log("launchSQL", err);
            })
        }
    }

}
module.exports = {con: db, launchSQL:launchSQL};