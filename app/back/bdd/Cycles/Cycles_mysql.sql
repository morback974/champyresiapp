CREATE DATABASE IF NOT EXISTS `CYCLES` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `CYCLES`;

CREATE TABLE `GESTIONAIR` (
  `id` VARCHAR(42),
  `mesuredat` VARCHAR(42),
  `temperatureair` VARCHAR(42),
  `consigneair` VARCHAR(42),
  `objair` VARCHAR(42),
  `pasair` VARCHAR(42),
  `varconsigneairactif` VARCHAR(42),
  `idcycle` VARCHAR(42),
  `date_début` VARCHAR(42),
  `date_fin` VARCHAR(42),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ACTIONHUM` (
  `id` VARCHAR(42),
  `deltahum` VARCHAR(42),
  `tempsdeshum` VARCHAR(42),
  `tempsbrume` VARCHAR(42),
  `idmesurehum` VARCHAR(42),
  `id_1` VARCHAR(42),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CYCLE` (
  `date_début` VARCHAR(42),
  `date_fin` VARCHAR(42),
  PRIMARY KEY (`date_début`, `date_fin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ACTIONAIR` (
  `id` VARCHAR(42),
  `launchedat` VARCHAR(42),
  `deltaair` VARCHAR(42),
  `etatvannefroidbefore` VARCHAR(42),
  `etatvannefroidafter` VARCHAR(42),
  `idmesureair` VARCHAR(42),
  `id_1` VARCHAR(42),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `GESTIONHUM` (
  `id` VARCHAR(42),
  `temperaturesec` VARCHAR(42),
  `temperaturehum` VARCHAR(42),
  `tauxhumidite` VARCHAR(42),
  `consignehum` VARCHAR(42),
  `objhum` VARCHAR(42),
  `pashum` VARCHAR(42),
  `objhumactif` VARCHAR(42),
  `date_début` VARCHAR(42),
  `date_fin` VARCHAR(42),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `GESTIONAIR` ADD FOREIGN KEY (`date_début`, `date_fin`) REFERENCES `CYCLE` (`date_début`, `date_fin`);
ALTER TABLE `ACTIONHUM` ADD FOREIGN KEY (`id_1`) REFERENCES `GESTIONHUM` (`id`);
ALTER TABLE `ACTIONAIR` ADD FOREIGN KEY (`id_1`) REFERENCES `GESTIONAIR` (`id`);
ALTER TABLE `GESTIONHUM` ADD FOREIGN KEY (`date_début`, `date_fin`) REFERENCES `CYCLE` (`date_début`, `date_fin`);