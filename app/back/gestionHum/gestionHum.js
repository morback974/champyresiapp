const {delay, hToMs} = require('../util/util');
// const timeInterval = require('../bdd/timeInterval');
const raspberry = require('../raspberry/raspberry');

let serveurMod = null;
let gestionHum = {};
let dataHum = null;
let gpioVentilo = configPin("gpioVentilo");
let gpioSec = configPin("gpioSec");
let gpioHum = configPin("gpioHum");
let gpioBrume = configPin("gpioBrume");

let timing = new Date();

gestionHum.launch = async () => {
    return new Promise(async (resolve, reject) => {
        try{
            if(!dataHum){
                // recup bdd valeur
                // TODO
                dataHum = {};
                dataHum = await initProxyHum();
            }

            await checkIfIsTime()
            .then(async () => {                     
                serveurMod = require('../serveur');
                serveurMod.status.suiviProcess = "Lancement Gestion Humidite";

                if(serveurMod.status.nbJour < 6){
                    // TODO faire Brume?
                }else{
                    await calculNewTauxHumidite();

                    let deltaHum = dataHum.tauxHumidite - dataHum.consigneHum;
                    console.log(dataHum);
                    if(deltaHum > 0){
                        // TODO Deshum
                        console.log("Humidité trop haute")
                    }else{
                        // TODO Brume
                        console.log("Humidité trop basse")
                    }
                }

                resolve();
            })
            .catch((err) => {
                if(err)
                    reject();
                else
                    resolve();
            })
        }catch(err){
            reject(err);
        }

    }).catch((err) => {
        console.log("launch hum", err);
    });
}

gestionHum.modifConsigneHum = (quantity) => {
    dataHum.consigneHum += quantity;
}

gestionHum.getLastTauxHumidite = () => {
    return new Promise((resolve, reject) => {
        let intHum = setInterval(() => {
            if(dataHum != null){
                if(dataHum.tauxHumidite){
                    clearInterval(intHum);
                    resolve(dataHum.tauxHumidite);
                }
            }
        });
    });
}

gestionHum.getConsigneHum = () => {
    return new Promise((resolve, reject) => {
        let intHum = setInterval(() => {
            if(dataHum != null){
                if(dataHum.consigneHum){
                    clearInterval(intHum);
                    resolve(dataHum.consigneHum);
                }
            }
        });
    });
}

gestionHum.getTemperatureSec = () => {
    return new Promise((resolve, reject) => {
        let intHum = setInterval(() => {
            if(dataHum != null){
                if(dataHum.temperatureSec){
                    clearInterval(intHum);
                    resolve(dataHum.temperatureSec);
                }
            }
        });
    });
}

gestionHum.getTemperatureHum = () => {
    return new Promise((resolve, reject) => {
        let intHum = setInterval(() => {
            if(dataHum != null){
                if(dataHum.temperatureHum){
                    clearInterval(intHum);
                    resolve(dataHum.getTemperatureHum());
                }
            }
        });
    });
}

gestionHum.initData = async () => {
    if(!dataHum){
        // recup bdd valeur
        // TODO
        dataHum = {};
        dataHum = await initProxyHum();
    }
}

gestionHum.getLastEtalHum = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataHum != null){
                if(dataHum.etalHum){
                    clearInterval(intAir);
                    resolve(dataHum.etalHum);
                }
            }
        });
    });
}

gestionHum.getLastEtalSec = () => {
    return new Promise((resolve, reject) => {
        let intAir = setInterval(() => {
            if(dataHum != null){
                if(dataHum.etalSec){
                    clearInterval(intAir);
                    resolve(dataHum.etalSec);
                }
            }
        });
    });
}

gestionHum.modifEtalHum = (quantity) => {
    dataHum.etalHum += quantity;
}

gestionHum.modifEtalSec = (quantity) => {
    dataHum.etalSec += quantity;
}

gestionHum.actifBrume = (val) => {
    raspberry.switchRelay(gpioBrume, 0, val);
}

gestionHum.desactifBrume = (val) => {
    raspberry.switchRelay(gpioBrume, 1);
}

function rajoutFunction(){
    dataHum.getTemperatureHum = () => {
        return dataHum.temperatureHum + dataHum.etalHum;
    }

    dataHum.getTemperatureSec = () => {
        return dataHum.temperatureSec + dataHum.etalSec;
    }
}

async function initProxyHum(){
    dataHum.tauxHumidite = 93;
    dataHum.temperatureSec = 13;
    dataHum.temperatureHum = 13;
    dataHum.consigneHum = 93;
    dataHum.objHum = 93;
    dataHum.pasHum = 0;
    dataHum.objHumActif = false;
    dataHum.idCycle = 1;
    dataHum.etalHum = 0;
    dataHum.etalSec = 0;

    rajoutFunction();

    return new Promise((resolve, reject) => {
        let socket = require('../routerSocket');

        resolve(new Proxy(dataHum, {
            set(obj, prop, value){
                if(prop == "tauxHumidite"){
                    socket.emit("tauxHumidite", value);
                    Reflect.set(...arguments);
                }else if(prop == "consigneHum"){
                    socket.emit("consigneHum", value);
                    Reflect.set(...arguments);
                }else if(prop == "temperatureSec"){
                    Reflect.set(...arguments);
                    socket.emit("temperatureSec", dataHum.getTemperatureSec());
                }else if(prop == "temperatureHum"){
                    Reflect.set(...arguments);
                    socket.emit("temperatureHum", dataHum.getTemperatureHum());
                }else if(prop == "etalHum"){
                    socket.emit("etalHum", value);
                    Reflect.set(...arguments);
                }else if(prop == "etalSec"){
                    socket.emit("etalSec", value);
                    Reflect.set(...arguments);
                }else{
                    Reflect.set(...arguments);
                }

                //saveGestionHum();
            }
        }));

    }).catch((err) => {
        console.log("initProxyHum", err);
    })
}

function modifVentilo(gpioVentilo, duration, message){
    return raspberry.switchRelay(gpioVentilo, 0, duration)
}

async function calculNewTauxHumidite(){
    return new Promise(async (resolve, reject) => {
        modifVentilo(gpioVentilo, 180);
        let tempSecAndHum = await Promise.all([mesureSec(), mesureHum()]);
        dataHum.temperatureSec = tempSecAndHum[0];
        console.log("Mesure Seche : ", dataHum.getTemperatureSec());
        dataHum.temperatureHum = tempSecAndHum[1];
        console.log("Mesure Humide : ", dataHum.getTemperatureHum());
    
        let pressSaturanteSec = calculPression(dataHum.getTemperatureSec());
        let pressSaturanteHum = calculPression(dataHum.getTemperatureHum());
    
        let pw = pressSaturanteHum - 1013 * 0.000662 * (dataHum.getTemperatureSec() - dataHum.getTemperatureHum());
        dataHum.tauxHumidite = pw/pressSaturanteSec * 100;
        console.log("Taux Humidite", dataHum.tauxHumidite)
        resolve(dataHum.tauxHumidite);
    }).catch((err) => {
        console.log("calculNewTauxHumidite", err);
    })

    function calculPression(temp){
        let pression = 0;
    
        let tabPressionSaturante = [12.28,12.364,12.448,12.532,12.616,12.7,12.784,12.868,12.952,13.036,13.12,13.21,13.3,13.39,13.48,13.57,13.66,13.75,13.84,13.93,14.02,14.115,14.21,14.305,14.4,14.495,14.59,14.685,14.78,14.875,14.97,15.071,15.172,15.273,15.374,15.475,15.576,15.677,15.778,15.879,
            15.98,16.087,16.194,16.301,16.408,16.515,16.622,16.729,16.836,16.943,17.05,17.163,17.276,17.389,17.502,17.615,17.728,17.841,17.954,18.067,18.18,18.299,18.418,18.537,18.656,18.775,18.894,19.013,19.132,19.251,19.37,19.496,19.622,19.748,19.874,20,20.126,20.252,20.378,20.504,20.63,20.764,20.898,
            21.032,21.166,21.3,21.434,21.568,21.702,21.836,21.97,22.111,22.252,22.393,22.534,22.675,22.816,22.957,23.098,23.239,23.38,23.529,23.678,23.827,23.976,24.125,24.274,24.423,24.572,24.721,24.87,25.026,25.182,25.338,25.494,25.65,25.806,25.962,26.118,26.274,26.43,26.596,26.762,26.928,27.094,
            27.26,27.426,27.592,27.758,27.924,28.09,28.264,28.438,28.612,28.786,28.96,29.134,29.308,29.482,29.656,29.83,30.014,30.198,30.382,30.566,30.75,30.934,31.118,31.302,31.486,31.67,31.863,32.056,32.249,32.442,32.635,32.828,33.021,33.214,33.407,33.6,33.804,34.008,34.212,34.416,34.62,34.824,35.028,
            35.232,35.436,35.64,35.856,36.072,36.288,36.504,36.72,36.936,37.152,37.368,37.584,37.8,38.025,38.25,38.475,38.7,38.925,39.15,39.375,39.6,39.825,40.05,40.288,40.526,40.764,41.002,41.24,41.478,41.716,41.954,42.192,42.43,42.679,42.928,43.177,43.426,43.675,43.924,44.173,44.422,44.671,44.92,
            45.183,45.446,45.709,45.972,46.235,46.498,46.761,47.024,47.287,47.55,47.825,48.1,48.375,48.65,48.925,49.2,49.475,49.75,50.025,50.3,50.589,50.878,51.167,51.456,51.745,52.034,52.323,52.612,52.901,53.19,53.494,53.798,54.102,54.406,54.71,55.014,55.318,55.622,55.926,56.23];
    
        let tempIterateur = 10;
        let finTempIterateur = 35;
        let iterateur = 0;
    
        while(tempIterateur <= finTempIterateur && pression == 0){
            if(temp > tempIterateur - 0.05 && temp <= tempIterateur + 0.05){
                pression = tabPressionSaturante[iterateur];
            }else{
                iterateur++;
                tempIterateur += 0.1;
            }
        }
        
        return pression;
    }
}

function mesureSec(){
    let raspberry = require('../raspberry/raspberry');
    return raspberry.gpioCan(gpioSec, 180)
    .then((valueMesure) => {
        valueMesure *= 5 * 40 / 5;

        if(valueMesure < 10 || valueMesure > 40){
            console.log("Probleme de Temperature Sec");
        }else{
            // resolve(Number(valueMesure));
            return Number(valueMesure);
        }
    })
    .catch((err) => {
        console.log(err, "Erreur Mesure Sec")
    });
}

async function mesureHum(){
    await delay(90, "seconde");
    let raspberry = require('../raspberry/raspberry');
    return raspberry.gpioCan(gpioHum, 90)    
    .then((valueMesure) => {
        valueMesure *= 5 * 40 / 5;

        if(valueMesure < 10 || valueMesure > 40){
            console.log("Probleme de Temperature Hum");
        }else{
            // resolve(Number(valueMesure));
            return Number(valueMesure);
        }
    })
    .catch((err) => {
        console.log(err, "Erreur Mesure Hum")
    }); 
}

function checkIfIsTime(){
    return new Promise((resolve, reject) => {
        if(Date.now() - timing > hToMs(1)){
            timing = new Date();
            resolve();
        }else{
            reject(err);
        }
    }).catch((err) => {
        //console.log("checkIfIsTime", err);
    })

    // return new Promise(async (resolve, reject) => {
    //     await timeInterval.getGestionHumidite().then(async (val) => {
    //         let lastGestionHumidite = Date.parse(val);
            
    //         // if(Date.now() - lastGestionHumidite > hToMs(1)){
    //             if(Date.now() - lastGestionHumidite > 1000){
    //             timeInterval.initGestionHumidite();
    //             resolve()
    //         }else{
    //             reject()
    //         }
    //     })
    // })
}

function configPin(pin){
    const fs = require('fs');
        
    let fichier = fs.readFileSync('./config/pin.json');
    let infoPin = JSON.parse(fichier);

    return infoPin.sensorTemperature[pin];
}

function saveGestionHum(){
    return new Promise((resolve, reject) => {
        let dataHumClean = {};
        if(dataHum.tauxHumidite < 80 || dataHum.tauxHumidite > 100 || !dataHum.tauxHumidite){
            console.log("Enregistrement DataHum Annulé cause Taux Humidite: " + dataHum.tauxHumidite);
            reject();
        }else{
            dataHumClean.tauxHumidite = dataHum.tauxHumidite;
        }
    
        if(dataHum.temperatureSec < 0 || dataHum.temperatureSec > 40 || !dataHum.temperatureSec){
            console.log("Enregistrement DataHum Annulé cause Temperature Sec: " + dataHum.temperatureSec);
            reject();
        }else{
            dataHumClean.temperatureSec = dataHum.temperatureSec;
        }

        if(dataHum.temperatureHum < 0 || dataHum.temperatureHum > 40 || !dataHum.temperatureHum){
            console.log("Enregistrement DataHum Annulé cause Temperature Hum: " + dataHum.temperatureHum);
            reject();
        }else{
            dataHumClean.temperatureHum = dataHum.temperatureHum;
        }

        if(dataHum.consigneHum < 80 || dataHum.consigneHum > 100 || !dataHum.consigneHum){
            console.log("Enregistrement DataAir Modifier cause consigne Hum: " + dataHum.consigneHum);
            console.log("Update Consigne Hum: 90");
            dataHum.consigneHum = 90;
            reject();
        }else{
            dataHumClean.consigneHum = dataHum.consigneHum;
        }
        
        if(dataHum.etalHum < -40 || dataHum.etalHum > 40 || dataHum.etalHum == null){
            console.log("Enregistrement DataAir Modifier cause etalonnage hum: " + dataHum.etalHum);
            console.log("Update etalonnage hum: 0");
            dataHum.etalHum = 0;
            reject();
        }else{
            dataHumClean.etalHum = dataHum.etalHum;
        }

        if(dataHum.etalSec < -40 || dataHum.etalSec > 40 || dataHum.etalSec == null){
            console.log("Enregistrement DataAir Modifier cause etalonnage sec: " + dataHum.etalSec);
            console.log("Update etalonnage sec: 0");
            dataHum.etalSec = 0;
            reject();
        }else{
            dataHumClean.etalSec = dataHum.etalSec;
        }

        resolve(dataHumClean);
    })
    .then((data) => {
        // let bddGestionHum = require('./../bdd/gestionHum');
        // bddGestionHum.insertGestionHum(data)
        // .then((val) => {
        //     //resolve(val.id);
        // });
    })
    .catch((err) => {
        
    })
}

module.exports = {gestionHum: gestionHum, dataHum: dataHum};