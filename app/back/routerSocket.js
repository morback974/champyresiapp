let io = null;

module.exports = {init: function(http){
    io = require('socket.io')(http, {
        cors: {
          origin: "*"
        }
      });

    io.on('connection', socket => {
    })
},
    emit: (label, data) => {
        io.emit(label, data);
    }
}