// const timeInterval = require('../bdd/timeInterval');
const {delay, hToMs} = require('../util/util');
const raspberry = require('../raspberry/raspberry');

let gestionCo2 = {};
let dataCo2 = null;
let pinVariateur = configPin("variateurCo2");

gestionCo2.launch = async () => {
    gestionCo2.initData();

    // await recupNextCycleCo2()
    // .then((duree) => {
    //     if(duree < 0){
    //         setTimeout(() => {
    //             updateTimeVariateur();
    //         }, 500);
    //     }else{
    //         setTimeout(() => {
    //             updateTimeVariateur();
    //         }, duree);
    //     }

    // });

    setTimeout(() => {
        updateTimeVariateur();
    }, 1000);
}

gestionCo2.getLastTauxCo2 = () => {
    return new Promise((resolve, reject) => {
        let intHum = setInterval(() => {
            if(dataCo2 != null){
                if(dataCo2.co2){
                    clearInterval(intHum);
                    resolve(dataCo2.co2);
                }
            }
        });
    });
}

gestionCo2.getConsigneCo2 = () => {
    return new Promise((resolve, reject) => {
        let intHum = setInterval(() => {
            if(dataCo2 != null){
                if(dataCo2.consigneCo2){
                    clearInterval(intHum);
                    resolve(dataCo2.consigneCo2);
                }
            }
        });
    });
}

gestionCo2.initData = async () => {
    if(!dataCo2){
        dataCo2 = {};
        dataCo2 = await initProxyCo2();
        launchCron();
    }
}

gestionCo2.modifConsigneCo2 = async (quantity) => {
    if(!dataCo2){
        dataCo2 = {};
        dataCo2 = await initProxyCo2();
        launchCron();
    }
    dataCo2.consigneCo2 += quantity;
}

async function updateTimeVariateur(){
    const fs = require('fs');
    const http = require('http');
    let fichier = fs.readFileSync('config/configCo2.json');
    let posteInfo = JSON.parse(fichier);

    new Promise((resolve, reject) => {
        http.get('http://' + posteInfo.ipMaster + ':6000/getCO2/' + posteInfo.room, (resp) => {
            let data = '';
    
            resp.on('data', (chunk) => {
                data += chunk;
                //console.log(data)
            })
    
            resp.on('end', async () => {
                data = JSON.parse(data);
                if(data){
                    dataCo2.co2 = data;
                    setTimeout(() => {
                        updateTimeVariateur();
                    // }, hToMs(1));
                }, 300000);

                const fs = require('fs');
        
                let fichier = fs.readFileSync('./gestionCo2/valueVariateur.json');
                let valVariateur = JSON.parse(fichier);

                if(dataCo2.co2 < dataCo2.consigneCo2 && dataCo2.freq == valVariateur.min){
                    dataCo2.dureeVariateurOff += 60000;
                        if(dataCo2.dureeVariateurOff > 300000){
                            dataCo2.dureeVariateurOff = 300000
                        }
                    }
                    //timeInterval.initGestionCo2();
                    resolve();
                }
            })
        }).on("error", (err) => {
            console.log("updateTimeVariateur", err);
            setTimeout(() => {
                updateTimeVariateur();
            }, 10000);
            reject("Acces au Serveur Co2 Master impossible (Mesure Co2)");
        });
    })
    .catch(err => {
        console.log("Erreur Co2", err);
    })
}

function recupNextCycleCo2(){
    return new Promise(async (resolve, reject) => {
        await timeInterval.getGestionCo2().then(async (val) => {
            let lastGestionCo2 = Date.parse(val);
            resolve(hToMs(1) - (Date.now() - lastGestionCo2));
        })
        .catch(() => {
            reject();
        })
    }).catch((err) => {
        console.log("recupNextCycleCo2", err);
    })
}

function initProxyCo2(){
    return new Promise((resolve, reject) => {
        dataCo2.dureeVariateurOff = 60000;
        dataCo2.consigneCo2 = 2500;
        dataCo2.co2 = 1500;
        dataCo2.freq = 204;
        let socket = require('../routerSocket');
    
        resolve(new Proxy(dataCo2, {
            set(obj, prop, value){
                if(prop == "co2"){
                    socket.emit("co2", value);
                    Reflect.set(...arguments);
                }else if(prop == "consigneCo2"){
                    socket.emit("consigneCo2", value);
                    Reflect.set(...arguments);
                }else{
                    Reflect.set(...arguments);
                }
            }
        }));
    }).catch((err) => {
        console.log("initProxyCo2", err);
    })

}

function launchCron(){
    setVariateur(0);
    setInterval(async () => {
        
        const fs = require('fs');
        
        let fichier = fs.readFileSync('./gestionCo2/valueVariateur.json');
        let valVariateur = JSON.parse(fichier);

        if(dataCo2.co2 > dataCo2.consigneCo2 && dataCo2.freq != valVariateur.min){
            dataCo2.dureeVariateurOff = 60000;
            dataCo2.freq += 5;
            if(dataCo2.freq > valVariateur.max){
                dataCo2.freq = valVariateur.max;
            }

            setVariateur(dataCo2.freq);
        }else if(dataCo2.co2 < dataCo2.consigneCo2 && dataCo2.freq != valVariateur.min){
            dataCo2.dureeVariateurOff = 60000;
            dataCo2.freq -= 5;
            if(dataCo2.freq < valVariateur.min){
                dataCo2.freq = valVariateur.min;
            }
            setVariateur(dataCo2.freq);
        }else{
            setVariateur(valVariateur.min);
            await delay(30, "seconde");
            setVariateur(0);
        }
    }, dataCo2.dureeVariateurOff);
}

function setVariateur(val){
    console.log("Activation variateur node ", val);

    if(process.platform != "win32"){
        const Gpio = require('pigpio').Gpio;

        const led = new Gpio(12, {mode: Gpio.OUTPUT});

        led.pwmWrite(val); 
        
        process.on('SIGINT', function () {
            led.digitalWrite(0);
        });
    }
}

function configPin(pin){
    const fs = require('fs');
        
    let fichier = fs.readFileSync('./config/pin.json');
    let infoPin = JSON.parse(fichier);

    return infoPin.sensorTemperature[pin];
}

module.exports = {gestionCo2: gestionCo2};