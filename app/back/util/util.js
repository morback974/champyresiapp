// attente en milliseconde
exports.delay = (duree, unite = "millis") => {
    switch(unite){
        case "minute": duree = duree * 60 * 1000; 
        break;

        case "seconde": duree = duree * 1000; 
        break;
    }

    return new Promise((resolve, reject) => {
        setTimeout(resolve, duree);
    });
}

// convertion heure to milliseconde
exports.hToMs = (h) => {
    return h * 60 * 60 * 1000;
}

exports.arrondi = (val) => {
    if(!isNaN(val)){
        return Math.round(val * 100) / 100;
    }
    else{
        return 0;
    }
};