const {spawn} = require('child_process');
let socket = require('../routerSocket');

let listRelayActif = [];

let cleanUp = spawn("python", ['raspberry/cleanUp.py']);

cleanUp.stdout.on('data', (data) => {
    //console.log("cleanUp.stdout", data)
});

cleanUp.stderr.on('data', (data) => {
    //console.log(data)
});

cleanUp.on('close', (code) => {
    // console.log(`Fin gpioCan ${gpio}`);
});

function getStateRelay(){
    listRelayActif.forEach(element => {
        switch (element) {
            case 41:
                socket.emit("relayVentilo", 1);
                break;
            
            case 15:
                socket.emit("relayVanneAir", 1);
                break;

            case 36:
                socket.emit("relayBrume", 1);
                break;

            default:
                break;
        }
    });

    if(listRelayActif.indexOf(41) == -1){
        socket.emit("relayVentilo", 0);
    }
    if(listRelayActif.indexOf(15) == -1){
        socket.emit("relayVanneAir", 0);
    }
    if(listRelayActif.indexOf(36) == -1){
        socket.emit("relayBrume", 0);
    }
}

function switchRelay(gpio, etat, duree = null){
    return new Promise((resolve, reject) => {
        console.log("Debut activation relay (" + gpio + "), duree " + duree, "etat", etat);
        if(listRelayActif.indexOf(gpio)){
            listRelayActif.push(gpio);
        }

        let python = null;

        if(process.platform == "win32"){
            if(etat == 0){
                python = spawn("python", ['raspberry/testScript.py', gpio]);
                switch (gpio) {
                    case 41:
                        socket.emit("relayVentilo", 1);
                        break;
        
                    case 15:
                        socket.emit("relayVanneAir", 1);
                        break;
        
                    case 36:
                        socket.emit("relayBrume", 1);
                        break;        
                    default:
                        break;
                }
                if(duree){
                    setTimeout(() => {
                        python = spawn("python", ['raspberry/testScript.py', gpio]);
                        switch (gpio) {
                            case 41:
                                socket.emit("relayVentilo", 0);
                                break;
                
                            case 15:
                                socket.emit("relayVanneAir", 0);
                                break;
                
                            case 36:
                                socket.emit("relayBrume", 0);
                                break;        
                            default:
                                break;
                        }
                    }, duree * 1000);
                }
            }
            else{
                python = spawn("python", ['raspberry/testScript.py', gpio]);
                switch (gpio) {
                    case 41:
                        socket.emit("relayVentilo", 0);
                        break;
        
                    case 15:
                        socket.emit("relayVanneAir", 0);
                        break;
        
                    case 36:
                        socket.emit("relayBrume", 0);
                        break;        
                    default:
                        break;
                }
            }
        }else{
            if(etat == 0){
                python = spawn("python", ['raspberry/relayOn.py', gpio]);
                switch (gpio) {
                    case 41:
                        socket.emit("relayVentilo", 1);
                        break;
        
                    case 15:
                        socket.emit("relayVanneAir", 1);
                        break;
        
                    case 36:
                        socket.emit("relayBrume", 1);
                        break;        
                    default:
                        break;
                }
                if(duree){
                    setTimeout(() => {
                        python = spawn("python", ['raspberry/relayOff.py', gpio]);
                        switch (gpio) {
                            case 41:
                                socket.emit("relayVentilo", 0);
                                break;
                
                            case 15:
                                socket.emit("relayVanneAir", 0);
                                break;
                
                            case 36:
                                socket.emit("relayBrume", 0);
                                break;        
                            default:
                                break;
                        }
                    }, duree * 1000);
                }
            }
            else{
                python = spawn("python", ['raspberry/relayOff.py', gpio]);
                switch (gpio) {
                    case 41:
                        socket.emit("relayVentilo", 0);
                        break;
        
                    case 15:
                        socket.emit("relayVanneAir", 0);
                        break;
        
                    case 36:
                        socket.emit("relayBrume", 0);
                        break;        
                    default:
                        break;
                }
            }
        }

        python.stdout.on('data', (data) => {
            console.log("Fin activation relay (" + gpio + ")");
            var index = listRelayActif.indexOf(gpio);
            if (index !== -1) {
                listRelayActif.splice(index, 1);
            }

            resolve();
        });

        python.stderr.on('data', (data) => {
            reject(data);
        });

        python.on('close', (data) => {
            // resolve();
        });
    }).catch((err) => {
        console.log("switchRelay", err.toString());
    })
}

function gpioCan(gpio, nbMesure){
    return new Promise((resolve, reject) => {
        let python = null;

        if(process.platform == "win32"){
            python = spawn("python", ['raspberry/testScript.py', gpio, nbMesure]);
        }else{
            python = spawn("python", ['raspberry/anal.py', gpio, nbMesure]);
        }

        python.stdout.on('data', (data) => {
            resolve(data.toString());
        });
        
        python.stderr.on('data', (data) => {
            reject(data);
        });
        
        python.on('close', (code) => {
            // console.log(`Fin gpioCan ${gpio}`);
        });
    }).catch((err) => {
        console.log("gpioCan", err.toString());
    })
}

function writePin(gpio, val, duree){
    return new Promise((resolve, reject) => {
        let python = null;

        if(process.platform == "win32"){
            python = spawn("python", ['raspberry/testScript.py', gpio, val]);
        }else{
            python = spawn("python", ['raspberry/variateur.py', gpio, val, duree]);
        }

        python.stdout.on('data', (data) => {
            resolve();
        });
        
        python.stderr.on('data', (data) => {
            reject(data);
        });
        
        python.on('close', (code) => {
            // console.log(`Fin gpioCan ${gpio}`);
        });
    }).catch((err) => {
        console.log("writePin", err.toString());
    })
}

module.exports = {switchRelay: switchRelay, gpioCan: gpioCan, getStateRelay: getStateRelay, writePin: writePin}