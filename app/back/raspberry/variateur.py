import RPi.GPIO as gpio
import time
from signal import signal, SIGINT
import sys
from sys import exit
 
def handler(signal_received, frame):
    print('')
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    gpio.cleanup()
    exit(0)
 
def main():
    gpio.setmode(gpio.BCM)
    gpio.setup(int(sys.argv[1]), gpio.OUT)
 
    p = gpio.PWM(int(sys.argv[1]), 50)
    p.start(int(sys.argv[2]))
    time.sleep(int(sys.argv[3]))
    p.stop()
    gpio.cleanup()
 
 
if __name__ == '__main__':
    signal(SIGINT, handler)
    main()