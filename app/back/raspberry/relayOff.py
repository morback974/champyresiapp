import RPi.GPIO as GPIO
import time
import sys

GPIO.setmode(GPIO.BOARD)

print(int(sys.argv[1]))

GPIO.setup(int(sys.argv[1]), GPIO.OUT)
GPIO.output(int(sys.argv[1]), GPIO.HIGH)

GPIO.cleanup()