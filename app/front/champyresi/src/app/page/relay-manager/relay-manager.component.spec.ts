import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelayManagerComponent } from './relay-manager.component';

describe('RelayManagerComponent', () => {
  let component: RelayManagerComponent;
  let fixture: ComponentFixture<RelayManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelayManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelayManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
