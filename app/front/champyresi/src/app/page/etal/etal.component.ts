import { Component, OnInit } from '@angular/core';
import { EtalService } from 'src/app/service/etal.service';

@Component({
  selector: 'app-etal',
  templateUrl: './etal.component.html',
  styleUrls: ['./etal.component.css']
})
export class EtalComponent implements OnInit {
  capteurAir = {libelle: "Air", ref: "etalAir", pin: ""};
  capteurHum = {libelle: "Hum", ref: "etalHum", pin: ""};
  capteurSec = {libelle: "Sec", ref: "etalSec", pin: ""};

  constructor(private _etalService: EtalService) {

    this._etalService.getConfigPin()
    .subscribe((pin:any) => {console.log(pin.sensorTemperature)
      this.capteurAir.pin = String(pin.sensorTemperature.gpioAir);
      this.capteurHum.pin = String(pin.sensorTemperature.gpioHum);
      this.capteurSec.pin = String(pin.sensorTemperature.gpioSec);
    })
  }

  ngOnInit(): void {

  }

}
