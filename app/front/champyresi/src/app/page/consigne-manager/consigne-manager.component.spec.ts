import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsigneManagerComponent } from './consigne-manager.component';

describe('ConsigneManagerComponent', () => {
  let component: ConsigneManagerComponent;
  let fixture: ComponentFixture<ConsigneManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsigneManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsigneManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
