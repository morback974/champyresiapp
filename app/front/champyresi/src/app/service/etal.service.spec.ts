import { TestBed } from '@angular/core/testing';

import { EtalService } from './etal.service';

describe('EtalService', () => {
  let service: EtalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EtalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
