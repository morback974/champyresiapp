import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private _httpClient: HttpClient, private _socket: Socket) { }

  getNbJour(){
    return this._httpClient.get<Number>("http://localhost:3000/getNbJour")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getSuiviProcess(){
    return this._httpClient.get<String>("http://localhost:3000/getSuiviProcess")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getSousSuiviProcess(){
    return this._httpClient.get<String>("http://localhost:3000/getSuiviSousProcess")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getLastNbJourOnChange(){
    return new Observable(observer => {
      this._socket.on("nbJour", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastSuiviProcessOnChange(){
    return new Observable(observer => {
      this._socket.on("suiviProcess", (data:string) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastSousSuiviProcessOnChange(){
    return new Observable(observer => {
      this._socket.on("suiviSousProcess", (data:string) => {
        observer.next(data);
      })

      return () => {}
    })
  }
}
