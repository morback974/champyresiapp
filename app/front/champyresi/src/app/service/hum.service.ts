import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HumService {

  constructor(private _httpClient: HttpClient, private _socket: Socket) { }

  getLastTauxHumidite(){
    return this._httpClient.get<Number>("http://localhost:3000/getLastTauxHumidite")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getConsigneHum(){
    return this._httpClient.get<Number>("http://localhost:3000/getConsigneHum")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getTemperatureSec(){
    return this._httpClient.get<Number>("http://localhost:3000/getTemperatureSec")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getTemperatureHum(){
    return this._httpClient.get<Number>("http://localhost:3000/getTemperatureHum")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getLastTauxHumiditeOnChange(){
    return new Observable(observer => {
      this._socket.on("tauxHumidite", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastConsigneHumOnChange(){
    return new Observable(observer => {
      this._socket.on("consigneHum", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastTemperatureSecOnChange(){
    return new Observable(observer => {
      this._socket.on("temperatureSec", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastTemperatureHumOnChange(){
    return new Observable(observer => {
      this._socket.on("temperatureHum", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  postModifConsigneHum(quantity:Number){
    return this._httpClient.post("http://localhost:3000/modifConsigneHum", {quantity: quantity})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }
}
