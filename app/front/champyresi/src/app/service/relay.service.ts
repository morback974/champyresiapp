import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class RelayService {
  configRelay:any = null;
  constructor(private _httpClient:HttpClient, private _socket: Socket) {   
    this._httpClient.get<Number>("http://localhost:3000/getConfigPin")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    )
    .subscribe(listRelay => {
      this.configRelay = listRelay;
    });
  }

  switchVentilo(){
    let relay = this.configRelay.sensorTemperature.ventilo;
    return this._httpClient.post("http://localhost:3000/switchRelay/", {"relay":relay, "etat":0, "duration":300})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  etatVanneAir(){
    return this._httpClient.get<Number>("http://localhost:3000/getVanneAir")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getStatusVentiloOnChange(){
    return new Observable(observer => {
      this._socket.on("relayVentilo", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  refreshState(){
    this._httpClient.get<Number>("http://localhost:3000/getStateRelay")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    ).subscribe(response => {
      
    });
  }

  modifVanneAir(val:any){
    return this._httpClient.post("http://localhost:3000/modifVanneAir/", {"val":val})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getStatusVanneAirOnChange(){
    return new Observable(observer => {
      this._socket.on("relayVanneAir", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getStatusBrumeOnChange(){
    return new Observable(observer => {
      this._socket.on("relayBrume", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  resetApp(){
    this._httpClient.get<Number>("http://localhost:3000/resetApp")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    ).subscribe(response => {
      
    });
  }

  actifBrume(val:number){
    return this._httpClient.post("http://localhost:3000/actifBrume/", {"val":val})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  desactifBrume(val:number){
    return this._httpClient.post("http://localhost:3000/desactifBrume/", {"val":val})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }
}
