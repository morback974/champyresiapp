import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, of} from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AirService {
  constructor(private _httpClient: HttpClient, private _socket: Socket) { }

  getLastTemperatureAir(){
    return this._httpClient.get<Number>("http://localhost:3000/getLastTemperatureAir")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getLastTemperatureAirOnChange(){
    return new Observable(observer => {
      this._socket.on("temperatureAir", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastConsigneAirOnChange(){
    return new Observable(observer => {
      this._socket.on("consigneAir", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastObjAirOnChange(){
    return new Observable(observer => {
      this._socket.on("objAir", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastPasAirOnChange(){
    return new Observable(observer => {
      this._socket.on("pasAir", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastStatusObjAirOnChange(){
    return new Observable(observer => {
      this._socket.on("varConsigneAirActif", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getConsigneAir(){
    return this._httpClient.get<Number>("http://localhost:3000/getConsigneAir")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getObjAir(){
    return this._httpClient.get<Number>("http://localhost:3000/getObjAir")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getPasAir(){
    return this._httpClient.get<Number>("http://localhost:3000/getPasAir")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getStatusObjAir(){
    return this._httpClient.get<Number>("http://localhost:3000/getStatusObjAir")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  postModifConsigneAir(quantity:Number){
    return this._httpClient.post("http://localhost:3000/modifConsigneAir/", {"quantity":quantity})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  setConsigneAir(value:Number){
    return this._httpClient.post("http://localhost:3000/setConsigneAir/", {"quantity":value})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  setPas(value:Number){
    return this._httpClient.post("http://localhost:3000/setPas/", {"quantity":value})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  setObjAir(value:Number){
    return this._httpClient.post("http://localhost:3000/setObjAir/", {"quantity":value})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }
}