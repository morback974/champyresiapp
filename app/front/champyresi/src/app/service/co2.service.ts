import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class Co2Service {

  constructor(private _httpClient: HttpClient, private _socket: Socket) { }

  getLastTauxCo2(){
    return this._httpClient.get<Number>("http://localhost:3000/getLastTauxCo2")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getConsigneCo2(){
    return this._httpClient.get<Number>("http://localhost:3000/getConsigneCo2")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getLastTauxCo2OnChange(){
    return new Observable(observer => {
      this._socket.on("co2", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  getLastConsigneCo2OnChange(){
    return new Observable(observer => {
      this._socket.on("consigneCo2", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  postModifConsigneCo2(quantity:Number){
    return this._httpClient.post("http://localhost:3000/modifConsigneCo2/", {"quantity":quantity})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }
}
