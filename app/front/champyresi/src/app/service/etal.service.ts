import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class EtalService {

  constructor(private _httpClient: HttpClient, private _socket: Socket) { }

  getConfigPin(){
    return this._httpClient.get<Number>("http://localhost:3000/getConfigPin")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getStatePin(pin:any){
    return this._httpClient.get<Number>("http://localhost:3000/getStateGpio/" + pin + "/1")
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getEtal(lib:string){
    return this._httpClient.get<Number>("http://localhost:3000/getEtal/" + lib)
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }

  getLastEtalOnChange(lib:string){
    return new Observable(observer => {
      this._socket.on(lib, (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  postModifEtal(lib: string, quantity:Number){
    return this._httpClient.post("http://localhost:3000/modifEtal/", {"lib":lib, "quantity":quantity})
    .pipe(
      catchError((error, observer) => {
          console.log("Acces au Serveur Champyresi impossible");
          return of([]);
      })
    );
  }
}
