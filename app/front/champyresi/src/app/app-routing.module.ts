import { Extractor } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatusComponent } from './components/status/status.component';
import { ConsigneManagerComponent } from './page/consigne-manager/consigne-manager.component';
import { EtalComponent } from './page/etal/etal.component';
import { HomeComponent } from './page/home/home.component';
import { RelayManagerComponent } from './page/relay-manager/relay-manager.component';

const routes: Routes = [
  {
    path:'', redirectTo:'home', pathMatch:'full'
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'etal',
    component:EtalComponent
  },
  {
    path:'consigneManager',
    component:ConsigneManagerComponent
  },
  {
    path:'relayManager',
    component:RelayManagerComponent
  },
  {
    path:'status',
    component:StatusComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
