import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'champyresi';

  constructor(private route:Router){}

  goToHome(){
    this.route.navigate(['home']);
  }

  goToEtal(){
    this.route.navigate(['etal']);
  }

  goToConsigneManager(){
    this.route.navigate(['consigneManager']);
  }

  goToRelayManager(){
    this.route.navigate(['relayManager']);
  }
}
