import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';
import { AirCardComponent } from './components/air-card/air-card.component';
import { HttpClientModule } from '@angular/common/http';
import { HumCardComponent } from './components/hum-card/hum-card.component';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { HomeCardComponent } from './components/home-card/home-card.component';
import { EtalComponent } from './page/etal/etal.component';
import { EtalCardComponent } from './components/etal-card/etal-card.component';
import { Co2CardComponent } from './components/co2-card/co2-card.component';
import { ConsigneManagerComponent } from './page/consigne-manager/consigne-manager.component';
import { ObjAirComponent } from './components/consigneManager/obj-air/obj-air.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RelayManagerComponent } from './page/relay-manager/relay-manager.component';
import { VentiloComponent } from './components/relay/ventilo/ventilo.component';
import { VanneAirComponent } from './components/relay/vanne-air/vanne-air.component';
import { BrumeComponent } from './components/relay/brume/brume.component';
import { StatusComponent } from './components/status/status.component';

const config: SocketIoConfig = {url: 'http://localhost:3000', options: {}};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AirCardComponent,
    HumCardComponent,
    HomeCardComponent,
    EtalComponent,
    EtalCardComponent,
    Co2CardComponent,
    ConsigneManagerComponent,
    ObjAirComponent,
    RelayManagerComponent,
    VentiloComponent,
    VanneAirComponent,
    BrumeComponent,
    StatusComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
