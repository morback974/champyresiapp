import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-status',
  template: '{{getProcessTime()}}'
})
export class StatusComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  getProcessTime(){
    return "{test:10}";
  }
}
