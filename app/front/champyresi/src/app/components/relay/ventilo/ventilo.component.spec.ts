import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentiloComponent } from './ventilo.component';

describe('VentiloComponent', () => {
  let component: VentiloComponent;
  let fixture: ComponentFixture<VentiloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentiloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VentiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
