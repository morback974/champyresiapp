import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { RelayService } from 'src/app/service/relay.service';

@Component({
  selector: 'app-ventilo',
  templateUrl: './ventilo.component.html',
  styleUrls: ['./ventilo.component.css']
})
export class VentiloComponent implements OnInit {
  isActif:boolean = true;

  constructor(private _relayService:RelayService) { }

  ngOnInit(): void {
    this.observeStatusVentilo();
  }

  switchVentilo(){
    this._relayService.switchVentilo().subscribe();
  }

  observeStatusVentilo(){
    this._relayService.getStatusVentiloOnChange().subscribe((response) => {
      this.isActif = Boolean(response);
    })
    setTimeout(() => {
      this._relayService.refreshState();
    })
  }

  resetApp(){
    this._relayService.resetApp();
  }
}