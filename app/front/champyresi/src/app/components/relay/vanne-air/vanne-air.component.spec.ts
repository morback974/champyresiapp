import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VanneAirComponent } from './vanne-air.component';

describe('VanneAirComponent', () => {
  let component: VanneAirComponent;
  let fixture: ComponentFixture<VanneAirComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VanneAirComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VanneAirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
