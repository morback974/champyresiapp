import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { RelayService } from 'src/app/service/relay.service';

@Component({
  selector: 'app-vanne-air',
  templateUrl: './vanne-air.component.html',
  styleUrls: ['./vanne-air.component.css']
})
export class VanneAirComponent implements OnInit {
  etatVanneAir:Number = 0;
  isActif:boolean = true;
  constructor(private _relayService:RelayService, private _socket: Socket) { }

  ngOnInit(): void {
    this._relayService.etatVanneAir()
    .subscribe(response => {
      this.etatVanneAir = Number(response);
      this.observeEtatvanneFroid().subscribe(response => {
        this.etatVanneAir = Number(response);
      });
    })

    this.observeStatusVentilo();
  }

  openVanne(val:any){
    this._relayService.modifVanneAir(val).subscribe();
  }

  closeVanne(val:any){
    this._relayService.modifVanneAir(-val).subscribe();
  }

  observeEtatvanneFroid(){
    return new Observable(observer => {
      this._socket.on("etatVanneFroid", (data:number) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  observeStatusVentilo(){
    this._relayService.getStatusVanneAirOnChange().subscribe((response) => {
      this.isActif = Boolean(response);
    })
    setTimeout(() => {
      this._relayService.refreshState();
    })
  }
}
