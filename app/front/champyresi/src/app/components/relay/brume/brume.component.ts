import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { RelayService } from 'src/app/service/relay.service';

@Component({
  selector: 'app-brume',
  templateUrl: './brume.component.html',
  styleUrls: ['./brume.component.css']
})
export class BrumeComponent implements OnInit {
  isActif:boolean = true;
  constructor(private _relayService:RelayService, private _socket: Socket) { }

  ngOnInit(): void {
    this.observeStatusBrume();
  }

  turnOn(){
    this._relayService.actifBrume(120).subscribe();
  }

  turnOff(){
    this._relayService.desactifBrume(0).subscribe();
  }

  observeStatusBrume(){
    this._relayService.getStatusBrumeOnChange().subscribe((response) => {
      this.isActif = Boolean(response);
    })
    setTimeout(() => {
      this._relayService.refreshState();
    })
  }

}
