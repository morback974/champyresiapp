import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrumeComponent } from './brume.component';

describe('BrumeComponent', () => {
  let component: BrumeComponent;
  let fixture: ComponentFixture<BrumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
