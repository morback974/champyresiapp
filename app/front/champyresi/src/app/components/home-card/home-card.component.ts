import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/service/home.service';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.css']
})
export class HomeCardComponent implements OnInit {
  nbJour:number = 0;
  etat:string = "";
  sousEtat:string = "";

  constructor(private _homeService: HomeService) { }

  ngOnInit(): void {
    this._homeService.getNbJour()
    .subscribe((response) => {
      this.nbJour = Number(response);
      this.observeNbJour();
    });

    this._homeService.getSuiviProcess()
    .subscribe((response) => {
      this.etat = String(response);
      this.observeSuiviProcess();
    });
    
    this._homeService.getSousSuiviProcess()
    .subscribe((response) => {
      this.sousEtat = String(response);
      this.observeSousSuiviProcess();
    });
  }

  observeNbJour(){
    this._homeService.getLastNbJourOnChange().subscribe((response) => {
      this.nbJour = Number(response);
    });
  }

  observeSuiviProcess(){
    this._homeService.getLastSuiviProcessOnChange().subscribe((response) => {
      this.etat = String(response);
    });
  }

  observeSousSuiviProcess(){
    this._homeService.getLastSousSuiviProcessOnChange().subscribe((response) => {
      this.sousEtat = String(response);
    });
  }
}
