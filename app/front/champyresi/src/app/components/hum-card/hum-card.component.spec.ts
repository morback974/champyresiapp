import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HumCardComponent } from './hum-card.component';

describe('HumCardComponent', () => {
  let component: HumCardComponent;
  let fixture: ComponentFixture<HumCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HumCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HumCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
