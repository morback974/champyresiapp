import { Component, OnInit } from '@angular/core';
import { HumService } from 'src/app/service/hum.service';

@Component({
  selector: 'app-hum-card',
  templateUrl: './hum-card.component.html',
  styleUrls: ['./hum-card.component.css']
})
export class HumCardComponent implements OnInit {
  tauxHumidite:number = 0;
  consigneHum:number = 0;
  temperatureSec:number = 0;
  temperatureHum:number = 0;

  constructor(private _humService: HumService) { }

  ngOnInit(): void {
    this._humService.getLastTauxHumidite()
    .subscribe((response) => {
      this.tauxHumidite = Math.round(Number(response)*100) / 100;
      this.observeTauxHumidite();
    });

    this._humService.getConsigneHum()
    .subscribe((response) => {
      this.consigneHum = Number(response);
      this.observeConsigneHum();
    });

    this._humService.getTemperatureSec()
    .subscribe((response) => {
      this.temperatureSec = Number(response);
      this.observeTemperatureSec();
    });

    this._humService.getTemperatureHum()
    .subscribe((response) => {
      this.temperatureHum = Number(response);
      this.observeTemperatureHum();
    });
  }

  renderTauxHumidite(){
    if(this.tauxHumidite)
      return Math.round(this.tauxHumidite * 100) / 100 + "%";
    else
      return "Loading...";
  }

  renderConsigneHum(){
    if(this.consigneHum)
      return this.consigneHum + "%";
    else
      return "Loading...";
  }

  renderTemperatureSec(){
    if(this.temperatureSec)
      return this.temperatureSec + "°C";
    else
      return "Loading...";
  }

  renderTemperatureHum(){
    if(this.temperatureHum)
      return this.temperatureHum + "°C";
    else
      return "Loading...";
  }

  upConsigneHum(quantity:Number){
    this._humService.postModifConsigneHum(quantity)
    .subscribe((rep) => {

    })
  }

  downConsigneHum(quantity:Number){
    this._humService.postModifConsigneHum(quantity)
    .subscribe((rep) => {
      
    })
  }

  observeTauxHumidite(){
    this._humService.getLastTauxHumiditeOnChange().subscribe((response) => {
      this.tauxHumidite = Number(response);
    });
  }

  observeConsigneHum(){
    this._humService.getLastConsigneHumOnChange().subscribe((response) => {
      this.consigneHum = Number(response);
    });
  }

  observeTemperatureSec(){
    this._humService.getLastTemperatureSecOnChange().subscribe((response) => {
      this.temperatureSec = Number(response);
    });
  }

  observeTemperatureHum(){
    this._humService.getLastTemperatureHumOnChange().subscribe((response) => {
      this.temperatureHum = Number(response);
    });
  }
}
