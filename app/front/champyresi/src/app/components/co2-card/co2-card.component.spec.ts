import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Co2CardComponent } from './co2-card.component';

describe('Co2CardComponent', () => {
  let component: Co2CardComponent;
  let fixture: ComponentFixture<Co2CardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Co2CardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Co2CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
