import { Component, OnInit } from '@angular/core';
import { Co2Service } from 'src/app/service/co2.service';

@Component({
  selector: 'app-co2-card',
  templateUrl: './co2-card.component.html',
  styleUrls: ['./co2-card.component.css']
})
export class Co2CardComponent implements OnInit {
  co2: number = 0;
  consigneCo2: number = 0;

  constructor(private _co2Service: Co2Service) { }

  ngOnInit(): void {
    this._co2Service.getLastTauxCo2()
    .subscribe((response) => {
      this.co2 = Number(response);
      this.observeCo2();
    })

    this._co2Service.getConsigneCo2()
    .subscribe((response) => {
      this.consigneCo2 = Number(response);
      this.observeConsigneCo2();
    })
  }

  observeCo2(){
    this._co2Service.getLastTauxCo2OnChange().subscribe((response) => {
      this.co2 = Number(response);
    });
  }

  observeConsigneCo2(){
    this._co2Service.getLastConsigneCo2OnChange().subscribe((response) => {
      this.consigneCo2 = Number(response);
    });
  }

  upConsigneCo2(quantity:Number){
    this._co2Service.postModifConsigneCo2(quantity)
    .subscribe((rep) => {

    })
  }

  downConsigneCo2(quantity:Number){
    this._co2Service.postModifConsigneCo2(quantity)
    .subscribe((rep) => {

    })
  }
}
