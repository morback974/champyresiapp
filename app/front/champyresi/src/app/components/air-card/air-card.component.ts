import { Component, OnInit, LOCALE_ID, Inject} from '@angular/core';
import { formatNumber } from '@angular/common';
import { AirService } from 'src/app/service/air.service';

@Component({
  selector: 'app-air-card',
  templateUrl: './air-card.component.html',
  styleUrls: ['./air-card.component.css']
})
export class AirCardComponent implements OnInit {
  temperatureAir:number = 0;
  consigneAir:number = 0;
  
  constructor(private _airService: AirService, @Inject(LOCALE_ID) private locale: string) { }

  ngOnInit(): void {
    this._airService.getLastTemperatureAir()
      .subscribe((response) => {
        this.temperatureAir = Number(response);
        this.observeTemperatureAir();
      });

    this._airService.getConsigneAir()
      .subscribe((response) => {
        this.consigneAir = Number(response);
        this.observeConsigneAir();
      })
  }

  renderTemperatureAir(){
    if(this.temperatureAir)
      return formatNumber(this.temperatureAir, this.locale, '1.0-2') + "°C";
    else
      return "Loading...";
  }

  renderConsigneAir(){
    if(this.consigneAir)
      return formatNumber(this.consigneAir, this.locale, '1.0-2') + "°C";
    else
      return "Loading...";
  }

  upConsigneAir(quantity:Number){
    this._airService.postModifConsigneAir(quantity)
    .subscribe((rep) => {

    })
  }

  downConsigneAir(quantity:Number){
    this._airService.postModifConsigneAir(quantity)
    .subscribe((rep) => {

    })
  }

  observeTemperatureAir(){
    this._airService.getLastTemperatureAirOnChange().subscribe((response) => {
      this.temperatureAir = Number(response);
    });
  }

  observeConsigneAir(){
    this._airService.getLastConsigneAirOnChange().subscribe((response) => {
      this.consigneAir = Number(response);
    });
  }
}