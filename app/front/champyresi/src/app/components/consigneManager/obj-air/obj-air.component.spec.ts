import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjAirComponent } from './obj-air.component';

describe('ObjAirComponent', () => {
  let component: ObjAirComponent;
  let fixture: ComponentFixture<ObjAirComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjAirComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjAirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
