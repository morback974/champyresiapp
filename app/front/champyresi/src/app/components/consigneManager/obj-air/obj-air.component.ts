import { Component, OnInit, LOCALE_ID, Inject} from '@angular/core';
import { AirService } from 'src/app/service/air.service';
import { formatNumber } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-obj-air',
  templateUrl: './obj-air.component.html',
  styleUrls: ['./obj-air.component.css']
})
export class ObjAirComponent implements OnInit {
  saisieDirectConsigneAir:any;
  saisiePas:any;
  saisiObjAir:any;
  temperatureAir:number = 0;
  consigneAir:number = 0;
  objAir:number = 0;
  pasAir:number = 0;
  statusObjAir:boolean = false;
  tempsRestant:String = "";

  constructor(private _airService: AirService,  @Inject(LOCALE_ID) private locale: string, private fb:FormBuilder) { }

  ngOnInit(): void {
    this.initForm();

    this._airService.getLastTemperatureAir()
    .subscribe((response) => {
      this.temperatureAir = Number(response);
      this.observeTemperatureAir();
    });

    this._airService.getConsigneAir()
    .subscribe((response) => {
      this.consigneAir = Number(response);
      this.observeConsigneAir();
    })

    this._airService.getObjAir()
    .subscribe((response) => {
      this.objAir = Number(response);
      this.observeObjAir();
    })

    this._airService.getPasAir()
    .subscribe((response) => {
      this.pasAir = Number(response);
      this.observePasAir();
    })

    this._airService.getStatusObjAir()
    .subscribe((response) => {
      this.statusObjAir = Boolean(response);
      this.observeStatusObjAir();
    })
  }

  initForm(){
    this.saisieDirectConsigneAir = this.fb.group({
      'consigneAir': ['']
    });

    this.saisiePas = this.fb.group({
      'pasAir': ['']
    });

    this.saisiObjAir = this.fb.group({
      'objAir': ['']
    })
  }

  renderTemperatureAir(){
    if(this.temperatureAir)
      return formatNumber(this.temperatureAir, this.locale, '1.0-2') + "°C";
    else
      return "Loading...";
  }

  renderConsigneAir(){
    if(this.consigneAir)
      return formatNumber(this.consigneAir, this.locale, '1.0-2') + "°C";
    else
      return "Loading...";
  }

  renderDureeRestante(){
    let dureeHeureTotal = 0;
    let nbJour = 0;
    let nbJourTotal = 0;
    let heure = 0;

    if(this.statusObjAir){
      dureeHeureTotal = Math.abs(this.objAir - this.consigneAir) / (this.pasAir / 12);
      nbJourTotal = dureeHeureTotal / 24;
      nbJour = Math.floor(nbJourTotal);
      heure = (dureeHeureTotal - nbJour)
      return this.tempsRestant = nbJour + " Jour et " + Math.round(heure) + " Heure";
    }else{
      return "Status Off";
    }
  }

  observeTemperatureAir(){
    this._airService.getLastTemperatureAirOnChange().subscribe((response) => {
      this.temperatureAir = Number(response);
    });
  }

  observeConsigneAir(){
    this._airService.getLastConsigneAirOnChange().subscribe((response) => {
      this.consigneAir = Number(response);
    });
  }

  observeObjAir(){
    this._airService.getLastObjAirOnChange().subscribe((response) => {
      this.objAir = Number(response);
    });
  }

  observePasAir(){
    this._airService.getLastPasAirOnChange().subscribe((response) => {
      this.pasAir = Number(response);
    });
  }

  observeStatusObjAir(){
    this._airService.getLastStatusObjAirOnChange().subscribe((response) => {
      this.statusObjAir = Boolean(response);
    });
  }

  setConsigneAir(){
    let consigneAir = Number(this.saisieDirectConsigneAir.value.consigneAir);
    if(consigneAir && consigneAir > 10 && consigneAir < 40){
      this._airService.setConsigneAir(consigneAir)
      .subscribe((rep) => {

      })
    }
  }

  setPas(){
    let pas = Number(this.saisiePas.value.pasAir);
    if(pas && pas > -40 && pas < 40){
      this._airService.setPas(pas)
      .subscribe((rep) => {

      })
    }
  }

  setObjAir(){
    let objAir = Number(this.saisiObjAir.value.objAir);
    if(objAir && objAir > 10 && objAir < 40){
      this._airService.setObjAir(objAir)
      .subscribe((rep) => {

      })
    }
  }

}
