import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtalCardComponent } from './etal-card.component';

describe('EtalCardComponent', () => {
  let component: EtalCardComponent;
  let fixture: ComponentFixture<EtalCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtalCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtalCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
