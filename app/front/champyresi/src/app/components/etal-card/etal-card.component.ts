import { Component, Input, OnInit, DoCheck} from '@angular/core';
import { Observable } from 'rxjs';
import { EtalService } from 'src/app/service/etal.service';

@Component({
  selector: 'app-etal-card',
  templateUrl: './etal-card.component.html',
  styleUrls: ['./etal-card.component.css']
})
export class EtalCardComponent implements OnInit, DoCheck{
  @Input() capteur:any;
  listTemp:Array<number> = [];
  temperature:number = 0;
  etallonage:number = 0;
  checkPin:boolean = false;

  constructor(private _etalService: EtalService) { }

  ngOnInit(): void {
    this._etalService.getEtal(this.capteur.ref)
    .subscribe((response) => {
      this.etallonage = Number(response);
      this.observeEtal(this.capteur.ref);
    });
  }
  
  ngDoCheck() {
    if(this.capteur.pin !== "" && !this.checkPin) {
      this.checkPin = !this.checkPin;

      const arrAvg = (arr:Array<number>) => arr.reduce((a,b) => a + b, 0) / arr.length
      this.getStateGpio().subscribe((val) => {
        this.listTemp.push(Number(val));
        if(this.listTemp.length > 0){
          this.temperature = arrAvg(this.listTemp);
        }
        
        if(this.listTemp.length > 30){
          this.listTemp.shift();
        }
      })
    }
 }

  getStateGpio(){
    return new Observable(observer => {
      this._etalService.getStatePin(this.capteur.pin)
      .subscribe((data) => {
        observer.next(data);
      })

      return () => {}
    })
  }

  map(value:number, x_min:number, x_max:number, y_min:number, y_max:number){
    return (value - x_min) * (y_max - y_min) / (x_max - x_min) + y_min;
  }

  observeEtal(lib:string){
    this._etalService.getLastEtalOnChange(lib).subscribe((response) => {
      this.etallonage = Number(response);
    });
  }

  modifEtal(quantity:number){
    this._etalService.postModifEtal(this.capteur.ref, quantity)
    .subscribe((rep) => {

    })
  }

  renderTemp(){
    return this.temperature + this.etallonage;
  }
}
