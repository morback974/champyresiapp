FROM node:buster

WORKDIR /usr/src/app

COPY app/back/. .

RUN npm install

CMD ["node", "serveur.js"]